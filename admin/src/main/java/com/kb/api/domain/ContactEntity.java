package com.kb.api.domain;

public class ContactEntity extends CommonEntity {
	int contact_seq;
	int common_seq; 
	int phone; 
	String email; 
	String content; 
	String file_url;
	String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getContact_seq() {
		return contact_seq;
	}
	public void setContact_seq(int contact_seq) {
		this.contact_seq = contact_seq;
	}
	public int getCommon_seq() {
		return common_seq;
	}
	public void setCommon_seq(int common_seq) {
		this.common_seq = common_seq;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getFile_url() {
		return file_url;
	}
	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}
	
	
	
}
