package com.kb.api.domain;


/*
Class Name : CommonEntity
Description : 怨듯넻肄붾뱶 媛앹껜
Modification Information

  �닔�젙�씪        �닔�젙�옄          �닔�젙�궡�슜
  -----------   ------------    ---------------------------
  2019.06.16  源��슦泥�         理쒖큹 �깮�꽦
	
*/
public class CommonEntity {
	int count;
	int rownum;
	private String create_dt;
	private String create_by;
	private String update_dt; 
	private String update_by;
	private String delete_yn;
	private String admin_seq;
	String show_yn;
	String create_dt_yyyymmdd;
	String update_dt_yyyymmdd;
	String payment_dt_yyyymmdd;
	String create_by_nm;
	String update_by_nm;
	String write_by;
	
	String table_name;
	String seq_name;
	String seq;
	
	
	String search_type;
	String search_value;
	 
	String start_1;
	String finish_1;
	
	
	int pg = 1;
	int page_block = 20;
	int page = 1;
	
	
	
	

	public String getAdmin_seq() {
		return admin_seq;
	}

	public void setAdmin_seq(String admin_seq) {
		this.admin_seq = admin_seq;
	}

	public String getPayment_dt_yyyymmdd() {
		return payment_dt_yyyymmdd;
	}

	public void setPayment_dt_yyyymmdd(String payment_dt_yyyymmdd) {
		this.payment_dt_yyyymmdd = payment_dt_yyyymmdd;
	}

	public String getTable_name() {
		return table_name;
	}

	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}

	public String getSeq_name() {
		return seq_name;
	}

	public void setSeq_name(String seq_name) {
		this.seq_name = seq_name;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	private int firstPageNo; // 泥� 踰덉㎏ �럹�씠吏� 踰덊샇
	private int prevPageNo; // �씠�쟾 �럹�씠吏� 踰덊샇
	private int startPageNo; // �떆�옉 �럹�씠吏� (�럹�씠吏� �꽕鍮� 湲곗�)
	private int endPageNo; // �걹 �럹�씠吏� (�럹�씠吏� �꽕鍮� 湲곗�)
	private int nextPageNo; // �떎�쓬 �럹�씠吏� 踰덊샇
	private int finalPageNo; // 留덉�留� �럹�씠吏� 踰덊샇
	private int totalCount; // 寃뚯떆湲� �쟾泥� �닔
	private int blockSize = 5; // �럹�씠吏� 踰덊샇 留곹겕 媛쒖닔
	private int startRowNum; // 寃뚯떆湲� 議고쉶 荑쇰━�뿉 �뱾�뼱媛� row �떆�옉�젏
	private int endRowNum; // 寃뚯떆湲� 議고쉶 荑쇰━�뿉 �뱾�뼱媛� row �걹�젏



	public int getFirstPageNo() {
		return firstPageNo;
	}

	public void setFirstPageNo(int firstPageNo) {
		this.firstPageNo = firstPageNo;
	}

	public int getPrevPageNo() {
		return prevPageNo;
	}

	public void setPrevPageNo(int prevPageNo) {
		this.prevPageNo = prevPageNo;
	}

	public int getStartPageNo() {
		return startPageNo;
	}

	public void setStartPageNo(int startPageNo) {
		this.startPageNo = startPageNo;
	}


	public int getEndPageNo() {
		return endPageNo;
	}

	public void setEndPageNo(int endPageNo) {
		this.endPageNo = endPageNo;
	}

	public int getNextPageNo() {
		return nextPageNo;
	}

	public void setNextPageNo(int nextPageNo) {
		this.nextPageNo = nextPageNo;
	}

	public int getFinalPageNo() {
		return finalPageNo;
	}

	public void setFinalPageNo(int finalPageNo) {
		this.finalPageNo = finalPageNo;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
		this.makePaging();
	}
	
	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}
	
	public int getStartRowNum() {
		return startRowNum;
	}

	public void setStartRowNum(int startRowNum) {
		this.startRowNum = startRowNum;
	}

	public int getEndRowNum() {
		return endRowNum;
	}

	public void setEndRowNum(int endRowNum) {
		this.endRowNum = endRowNum;
	}

	private void makePaging() {
		if (this.totalCount == 0)
			return; // 寃뚯떆湲� �쟾泥� �닔媛� �뾾�뒗 寃쎌슦
		if (this.page == 0)
			this.setPage(1); // 湲곕낯 媛� �꽕�젙
		if (this.page_block == 0)
			this.setPage_block(10); // 湲곕낯 媛� �꽕�젙

		int finalPage = (totalCount + (page_block - 1)) / page_block; // 留덉�留� �럹�씠吏�
		if (this.page > finalPage)
			this.setPage(finalPage); // 湲곕낯 媛� �꽕�젙

		if (this.page < 0 || this.page > finalPage)
			this.page = 1; // �쁽�옱 �럹�씠吏� �쑀�슚�꽦 泥댄겕

		boolean isNowFirst = page == 1 ? true : false; // �떆�옉 �럹�씠吏� (�쟾泥�)
		boolean isNowFinal = page == finalPage ? true : false; // 留덉�留� �럹�씠吏� (�쟾泥�)

		int startPage = ((page - 1) / blockSize) * blockSize + 1; // �떆�옉 �럹�씠吏� (�럹�씠吏� �꽕鍮� 湲곗�)
		int endPage = startPage + blockSize - 1; // �걹 �럹�씠吏� (�럹�씠吏� �꽕鍮� 湲곗�)

		if (endPage > finalPage) { // [留덉�留� �럹�씠吏� (�럹�씠吏� �꽕鍮� 湲곗�) > 留덉�留� �럹�씠吏�]蹂대떎 �겙 寃쎌슦
			endPage = finalPage;
		}

		this.setFirstPageNo(1); // 泥� 踰덉㎏ �럹�씠吏� 踰덊샇

		if (isNowFirst) {
			this.setPrevPageNo(1); // �씠�쟾 �럹�씠吏� 踰덊샇
		} else {
			this.setPrevPageNo(((page - 1) < 1 ? 1 : (page - 1))); // �씠�쟾 �럹�씠吏� 踰덊샇
		}

		this.setStartPageNo(startPage); // �떆�옉 �럹�씠吏� (�럹�씠吏� �꽕鍮� 湲곗�)
		this.setEndPageNo(endPage); // �걹 �럹�씠吏� (�럹�씠吏� �꽕鍮� 湲곗�)

		if (isNowFinal) {
			this.setNextPageNo(finalPage); // �떎�쓬 �럹�씠吏� 踰덊샇
		} else {
			this.setNextPageNo(((page + 1) > finalPage ? finalPage : (page + 1))); // �떎�쓬 �럹�씠吏� 踰덊샇
		}

		this.setFinalPageNo(finalPage); // 留덉�留� �럹�씠吏� 踰덊샇
	}
	
	
	
	

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
		this.setEndRowNum(page * page_block);
		this.setStartRowNum(endRowNum - (page_block - 1));	
	}

	public int getPg() {
		return pg;
	}
	
	public void setPg(int pg) {
		this.pg = pg;
	}


	public int getPage_block() {
		return page_block;
	}

	public void setPage_block(int page_block) {
		this.page_block = page_block;
	}

	public String getSearch_type() {
		return search_type;
	}
	public void setSearch_type(String search_type) {
		this.search_type = search_type;
	}
	public String getSearch_value() {
		return search_value;
	}
	public void setSearch_value(String search_value) {
		this.search_value = search_value;
	}
	public String getCreate_by_nm() {
		return create_by_nm;
	}
	public void setCreate_by_nm(String create_by_nm) {
		this.create_by_nm = create_by_nm;
	}
	public String getUpdate_by_nm() {
		return update_by_nm;
	}
	public void setUpdate_by_nm(String update_by_nm) {
		this.update_by_nm = update_by_nm;
	}
	public int getRownum() {
		return rownum;
	}
	public void setRownum(int rownum) {
		this.rownum = rownum;
	}
	public String getWrite_by() {
		return write_by;
	}
	public void setWrite_by(String write_by) {
		this.write_by = write_by;
	}
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	public String getCreate_by() {
		return create_by;
	}
	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	public String getUpdate_dt() {
		return update_dt;
	}
	public void setUpdate_dt(String update_dt) {
		this.update_dt = update_dt;
	}
	public String getUpdate_by() {
		return update_by;
	}
	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}
	public String getDelete_yn() {
		return delete_yn;
	}
	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}
	public String getShow_yn() {
		return show_yn;
	}
	public void setShow_yn(String show_yn) {
		this.show_yn = show_yn;
	}
	public String getCreate_dt_yyyymmdd() {
		return create_dt_yyyymmdd;
	}
	public void setCreate_dt_yyyymmdd(String create_dt_yyyymmdd) {
		this.create_dt_yyyymmdd = create_dt_yyyymmdd;
	}
	public String getUpdate_dt_yyyymmdd() {
		return update_dt_yyyymmdd;
	}
	public void setUpdate_dt_yyyymmdd(String update_dt_yyyymmdd) {
		this.update_dt_yyyymmdd = update_dt_yyyymmdd;
	}

	public String getStart_1() {
		return start_1;
	}

	public void setStart_1(String start_1) {
		this.start_1 = start_1;
	}

	public String getFinish_1() {
		return finish_1;
	}

	public void setFinish_1(String finish_1) {
		this.finish_1 = finish_1;
	}
	
	
}
