package com.kb.api.domain;

public class Meta {

	private int result_code = 200;
	private String result_msg = "success";
	private String result_view = null;
	private String result_type = "json";
	private String result_explain = "성공";
	private String result_target = null;
	
	
	
	public String getResult_target() {
		return result_target;
	}
	public void setResult_target(String result_target) {
		this.result_target = result_target;
	}
	public String getResult_explain() {
		return result_explain;
	}
	public void setResult_explain(String result_explain) {
		this.result_explain = result_explain;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public int getResult_code() {
		return result_code;
	}
	public void setResult_code(int result_code) {
		this.result_code = result_code;
	}
	public String getResult_msg() {
		return result_msg;
	}
	public void setResult_msg(String result_msg) {
		this.result_msg = result_msg;
	}
	public String getResult_view() {
		return result_view;
	}
	public void setResult_view(String result_view) {
		this.result_view = result_view;
	}
	
	
	
}
