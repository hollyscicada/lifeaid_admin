package com.kb.api.domain;

public class CommonCodeEntity extends CommonEntity {
	int common_seq;
    String name;
    String code;
    String sub_code;
    int sort;
    
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public int getCommon_seq() {
		return common_seq;
	}
	public void setCommon_seq(int common_seq) {
		this.common_seq = common_seq;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSub_code() {
		return sub_code;
	}
	public void setSub_code(String sub_code) {
		this.sub_code = sub_code;
	}
    
    
}
