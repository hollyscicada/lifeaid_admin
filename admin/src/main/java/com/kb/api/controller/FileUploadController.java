package com.kb.api.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping({"/api/file/*"})
public class FileUploadController {

	  String setCharEncoding = "UTF-8";
	    public static String UPLOAD_PATH = "/resources/images/";
	    @Value("#{props['domain.url']}")
	    String domain_url;

	    public FileUploadController() {
	    }

	    public String fileupload(MultipartFile file1, HttpServletRequest request) {
	        try {
	            if (!file1.isEmpty()) {
	                String fileName = "";
	                String strToday = simpleDate("yyyyMMddkms");
	                String path = request.getServletContext().getRealPath(UPLOAD_PATH);
	                fileName = file1.getOriginalFilename();
	                String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
	                if (checkLang(fileName)) {
	                    fileName = UUID.randomUUID().toString().replace("-", "") + "." + ext;
	                }

	                fileName = strToday + "_" + fileName;
	                file1.transferTo(new File(path + fileName));
	                return this.domain_url + UPLOAD_PATH + fileName;
	            }
	        } catch (Exception var7) {
	            var7.printStackTrace();
	        }

	        return null;
	    }

	    public static void mkDir(String realPath) {
	        File updir = new File(realPath);
	        if (!updir.exists()) {
	            updir.mkdirs();
	        }

	    }

	    public static String simpleDate(String fommat) {
	        SimpleDateFormat sdf = new SimpleDateFormat(fommat);
	        Calendar c1 = Calendar.getInstance();
	        String strToday = sdf.format(c1.getTime());
	        return strToday;
	    }

	    public static boolean checkLang(String str) {
	        return str.matches(".*[��-����-�Ӱ�-�R]+.*");
	    }

	    

	    
	    @RequestMapping({"/upload_binary"})
	    @ResponseBody
	    private String file_uploader(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	        request.setCharacterEncoding("utf-8");
	        String realname = request.getHeader("file-name");
	        String subPath = UPLOAD_PATH + "admin" + "/";
	        String path = request.getServletContext().getRealPath(subPath);
	        mkDir(path);
	        InputStream is = request.getInputStream();
	        OutputStream os = new FileOutputStream(path + realname);
	        byte[] b = new byte[Integer.parseInt(request.getHeader("file-size"))];

	        int numRead;
	        while((numRead = is.read(b, 0, b.length)) != -1) {
	            os.write(b, 0, numRead);
	        }

	        if (is != null) {
	            is.close();
	        }

	        os.flush();
	        os.close();
	        return subPath + realname;
	    }

	    @RequestMapping({"/upload_multipart"})
	    @ResponseBody
	    private String file_uploader2(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	        response.setCharacterEncoding(this.setCharEncoding);
	        response.setContentType("text/html; charset=" + this.setCharEncoding + " \" ");
	        String realPath = request.getServletContext().getRealPath(UPLOAD_PATH);
	        String filename = this.multipart_upload(request, realPath);
	        PrintWriter out = response.getWriter();
	    	String callback = request.getParameter("CKEditorFuncNum");
	    	String fileUrl = request.getContextPath() + "/images/" + filename;
//	        out.println("<script>");
//	        out.println("opener.pasteHTML('" + filename + "');");
//	        out.println("self.close();");
//	        out.println("</script>");
	        out.println("<script>window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + fileUrl + "','�̹����� ���ε�Ǿ����ϴ�.')" + "</script>");
	        out.flush();
	        return null;
	    }

	    @PostMapping({"/ajaxupload"})
	    @ResponseBody
	    private String ajaxupload(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) throws Exception {
	        response.setContentType("text/html; charset=" + this.setCharEncoding + " \" ");
	        String subPath = request.getParameter("sub_path");
	        subPath = UPLOAD_PATH + subPath + "/";
	        String realPath = request.getServletContext().getRealPath(subPath);
	        String filename = this.multipart_upload(request, realPath);
	        return subPath + filename;
	    }

	    public String multipart_upload(HttpServletRequest request, String realPath) throws IllegalStateException, IOException {
	        mkDir(realPath);
	        String filename = "";
	        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
	        Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
	        MultipartFile mrequest = null;

	        while(iterator.hasNext()) {
	            mrequest = multipartHttpServletRequest.getFile((String)iterator.next());
	            String filenameTemp = mrequest.getOriginalFilename();
	            String ext = filenameTemp.substring(filenameTemp.lastIndexOf(".") + 1);
	            filename = filename + UUID.randomUUID().toString().replace("-", "") + "." + ext;
	            if (!mrequest.isEmpty()) {
	                mrequest.transferTo(new File(realPath + filename));
	            }
	        }

	        return filename;
	    }

	    @PostMapping({"/ckeditUpload"})
	    @ResponseBody
	    public void chekUpload(HttpServletRequest request, HttpServletResponse response,MultipartHttpServletRequest muHttpServletRequests, @RequestParam MultipartFile multi) throws Exception {
	    	//UUID uid = UUID.randomUUID();
	    	
	    	response.setCharacterEncoding("utf-8");
	    	response.setContentType("text/html; charset=utf-8");
	    	
	    	String fileName = multi.getOriginalFilename();
	    	
	    	byte[] bytes = multi.getBytes();
	    	
	    	String uploadPath = "C:\\Users\\leanauto\\Desktop\\lifeaid\\lifeaid_admin\\admin\\src\\main\\webapp\\resources\\img\\images\\";
	    	
	    	OutputStream out = new FileOutputStream(new File(uploadPath + fileName));
	    	
	    	out.write(bytes);
	    	
	    	String callback = request.getParameter("CKEditorFuncNum");
	    	
	    	PrintWriter printWriter = response.getWriter();
	    	String fileUrl = request.getContextPath() + "/images/" + fileName;
	    	
	    	printWriter.print("<script>window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + fileUrl
	                + "','�̹����� ���ε�Ǿ����ϴ�.')" + "</script>");
	        printWriter.flush();
	    }
	    

}
