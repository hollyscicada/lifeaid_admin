package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.AdminEntity;
import com.kb.api.service.MasterService;


@Controller
@RequestMapping(value = "/supervise/admin/**")
public class AdminsController {

	private static final Logger logger = LoggerFactory.getLogger(AdminsController.class);
	 
	@Inject MasterService masterService;
	
	
	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<AdminEntity> list =  (List<AdminEntity>) masterService.dataList("mapper.admin.AdminMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/admin/list";
	}
	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
	public String AdminDetailGET(Model model , @PathVariable String seq ) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.AdminMapper", "detail", seq));
		return "adminLayout/admin/admin/read";
	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String AdminAddGET(Model model) throws Exception {
		return "adminLayout/admin/admin/create";
	}
	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
	public String AdminUpdateGET(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.AdminMapper", "detail", seq));
		return "adminLayout/admin/admin/update";
	}
	
}
