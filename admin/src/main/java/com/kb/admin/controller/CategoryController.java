package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.CategoryEntity;
import com.kb.api.service.MasterService;




@Controller
@RequestMapping(value = "/supervise/category/**")
public class CategoryController {
	
	private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);
	 
	@Inject MasterService masterService;
	

	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<CategoryEntity> list =  (List<CategoryEntity>) masterService.dataList("mapper.admin.CategoryMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/category/list";
	}
	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
	public String CategoryDetailGET(Model model , @PathVariable String seq ) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.CategoryMapper", "detail", seq));
		return "adminLayout/admin/category/read";
	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String CategoryAddGET(Model model) throws Exception { 
		return "adminLayout/admin/category/create";
	}
//	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
//	public String CategoryUpdateGET(Model model, @PathVariable String seq) throws Exception {
//		model.addAttribute("detail", masterService.dataRead("mapper.admin.CategoryMapper", "detail", seq));
//		return "adminLayout/admin/category/update";
//	}
	@RequestMapping(value = {"/update"} , method = RequestMethod.GET)
	public String CategoryUpdateGET(Model model) throws Exception {

		List<CategoryEntity> category =  (List<CategoryEntity>) masterService.dataList("mapper.admin.CategoryMapper", "list", null);
		model.addAttribute("category", category);
		return "adminLayout/admin/category/update";
	}
}
