package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.CommentEntity;

import com.kb.api.service.MasterService;

@Controller
@RequestMapping(value = "/supervise/comment/**")
public class CommentController {

	private static final Logger logger = LoggerFactory.getLogger(CommentController.class);
	 
	@Inject MasterService masterService;
	
	
	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<CommentEntity> list =  (List<CommentEntity>) masterService.dataList("mapper.admin.CommentMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/comment/list";
	}
}
