package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.CategoryEntity;
import com.kb.admin.domain.ContentsEntity;
import com.kb.api.service.MasterService;

@Controller
@RequestMapping(value = "/supervise/contents/**")
public class ContentsController {


	private static final Logger logger = LoggerFactory.getLogger(ContentsController.class);
	 
	@Inject MasterService masterService;
	

	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<ContentsEntity> list =  (List<ContentsEntity>) masterService.dataList("mapper.admin.ContentsMapper", "list", null);
		model.addAttribute("list", list);
		List<CategoryEntity> category =  (List<CategoryEntity>) masterService.dataList("mapper.admin.CategoryMapper", "list", null);
		model.addAttribute("category", category);
		return "adminLayout/admin/contents/list";
	}
	
	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
	public String ContentDetailGET(Model model , @PathVariable String seq ) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.ContentsMapper", "detail", seq));
		return "adminLayout/admin/contents/read";
	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String ContentAddGET(Model model) throws Exception {
		return "adminLayout/admin/contents/create";
	}
	
	@RequestMapping(value = {"/select"} , method = RequestMethod.GET)
	public String ContentSelectGET(Model model) throws Exception {
		return "adminLayout/admin/contents/select";
	}
	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
	public String ContentUpdateGET(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.ContentsMapper", "detail", seq));
		return "adminLayout/admin/contents/update";
	}

}
