package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.CategoryEntity;
import com.kb.admin.domain.MemberEntity;
import com.kb.api.service.MasterService;

@Controller
@RequestMapping(value = "/supervise/member/**")
public class MemberController {


	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	 
	@Inject MasterService masterService;
	

	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, MemberEntity memberEntity) throws Exception {

		List<MemberEntity> list =  (List<MemberEntity>) masterService.dataList("mapper.admin.MemberMapper", "list", null);
		model.addAttribute("list", list);
		

		List<CategoryEntity> category =  (List<CategoryEntity>) masterService.dataList("mapper.admin.CategoryMapper", "list", null);
		model.addAttribute("category", category);
		return "adminLayout/admin/member/list";
	}
	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
	public String MemberDetailGET(Model model , @PathVariable String seq ) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.MemberMapper", "detail", seq));
		model.addAttribute("payment", masterService.dataRead("mapper.admin.PaymentMapper", "payment", seq));
		return "adminLayout/admin/member/read";
	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String MemberAddGET(Model model) throws Exception {
		return "adminLayout/admin/member/create";
	}
	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
	public String MemberUpdateGET(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.MemberMapper", "detail", seq));
		return "adminLayout/admin/member/update";
	}
	                            
}
