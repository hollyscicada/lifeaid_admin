package com.kb.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kb.admin.domain.MemberEntity;
import com.kb.api.service.MasterService;
import com.kb.api.util.Common;

@RestController
@RequestMapping(value = "/supervise/admin/v1/member/**")
public class MemberRestController {


	@Inject MasterService masterService;
	
	@PostMapping("/create")
	public ResponseEntity<Map<String, Object>> dashboard(HttpServletRequest request, @RequestBody MemberEntity memberEntity) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		int record = masterService.dataCreate("mapper.admin.MemberMapper", "create", memberEntity);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
	@PostMapping("/search")
	public ResponseEntity<Map<String, Object>> listdashboard(HttpServletRequest request,  Model model, @RequestBody MemberEntity memberEntity) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		int record = masterService.dataList("mapper.admin.MemberMapper", "search", memberEntity).size();

//		System.out.println("memberEntity.getSearch_type :: "+ memberEntity.getSearch_type());
//		System.out.println("memberEntity.getSearch_value :: "+ memberEntity.getSearch_value());
//		System.out.println("record :: "+ record);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		List<MemberEntity> list = (List<MemberEntity>)masterService.dataList("mapper.admin.MemberMapper", "search", memberEntity);
		System.out.println(list.size());
		System.out.println(list.size());
		System.out.println(list.size());
		System.out.println(list.size());
		System.out.println(list.size());
		
		map.put("data", list); 

		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
	
	@PostMapping("/update")
	public ResponseEntity<Map<String, Object>> update(HttpServletRequest request, @RequestBody MemberEntity memberEntity) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		int record = masterService.dataUpdate("mapper.admin.MemberMapper", "update", memberEntity);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	@PostMapping("/update_pre")
	public ResponseEntity<Map<String, Object>> update_pre(HttpServletRequest request, @RequestBody MemberEntity memberEntity) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		int record = masterService.dataUpdate("mapper.admin.MemberMapper", "update_pre", memberEntity);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
	@GetMapping("/remove")
	public ResponseEntity<Map<String, Object>> removeDELETE(@RequestParam(value = "seq" , required = false) String seq) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;		Map<String, Object> map = new HashMap<String, Object>();
		
		int record = masterService.dataCreate("mapper.admin.MemberMapper", "remove", seq);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
}
