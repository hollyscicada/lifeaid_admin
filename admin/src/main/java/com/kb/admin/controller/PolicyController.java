package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.PolicyEntity;
import com.kb.api.service.MasterService;



@Controller
public class PolicyController {
	private static final Logger logger = LoggerFactory.getLogger(PolicyController.class);
	 
	@Inject MasterService masterService;
	 
	
	@RequestMapping(value ={ "/supervise/policy/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<PolicyEntity> list =  (List<PolicyEntity>) masterService.dataList("mapper.admin.PolicyMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/policy/list";
	}
	
	@RequestMapping(value ={ "/supervise/info/list" }, method = RequestMethod.GET)
	public String dashboardInfo(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<PolicyEntity> list =  (List<PolicyEntity>) masterService.dataList("mapper.admin.PolicyMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/info/list";
	}
}
