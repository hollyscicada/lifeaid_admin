package com.kb.admin.controller;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kb.admin.domain.AdminEntity;
import com.kb.admin.domain.CSEntity;
import com.kb.admin.domain.LoginEntity;
import com.kb.admin.domain.MemberEntity;
import com.kb.admin.domain.PaymentEntity;
import com.kb.api.service.MasterService;
import com.kb.api.util.Common;


@Controller
public class LoginsController {
	@Inject MasterService masterService;
//	@Inject DashboardService DashboardService;

	private static final Logger logger = LoggerFactory.getLogger(LoginsController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws Exception 
	 */
	/*愿�由ъ옄 �쁽�솴�뙋*/
	@RequestMapping(value ={ "/" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		String loginCheck = (String) session.getAttribute("admin_seq");
		
		if(loginCheck != null && !"".equals(loginCheck)) {   
			List<PaymentEntity> payment =  (List<PaymentEntity>) masterService.dataList("mapper.admin.PaymentMapper", "list", null);
			model.addAttribute("payment", payment);
			List<CSEntity> cs =  (List<CSEntity>) masterService.dataList("mapper.admin.CSMapper", "list", null);
			model.addAttribute("cs", cs);
			model.addAttribute("payment_count", masterService.dataCount("mapper.admin.PaymentMapper", "count", null));
			
			model.addAttribute("content_count", masterService.dataCount("mapper.admin.ContentsMapper", "countData", null));
			
			model.addAttribute("e_content_count", masterService.dataCount("mapper.admin.ContentsMapper", "e_countData", null));
			
			model.addAttribute("pre_member_count", masterService.dataCount("mapper.admin.MemberMapper", "count_pre", null));
			
			
			return "adminLayout/admin/index";
		}else {
			return "adminLayoutNotLogin/admin/login";
		}
	}  

	/**
	 * @msg : 濡쒓렇�븘�썐
	 */
	@RequestMapping(value = {"/supervise/logout"}, method =  { RequestMethod.GET , RequestMethod.POST })
	public String logout(Model model, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		session.invalidate();
		return "redirect:/";
	}
}
