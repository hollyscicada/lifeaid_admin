package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.FaqEntity;
import com.kb.api.service.MasterService;


@Controller
@RequestMapping(value = "/supervise/faq/**")
public class FaqsController {

	private static final Logger logger = LoggerFactory.getLogger(FaqsController.class);
	 
	@Inject MasterService masterService;
	
	
	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<FaqEntity> list =  (List<FaqEntity>) masterService.dataList("mapper.admin.FaqMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/faq/list";
	}
	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
	public String AdminDetailGET(Model model , @PathVariable String seq ) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.FaqMapper", "detail", seq));
		return "adminLayout/admin/faq/read";
	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String AdminAddGET(Model model) throws Exception {
		return "adminLayout/admin/faq/create";
	}
	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
	public String AdminUpdateGET(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.FaqMapper", "detail", seq));
		return "adminLayout/admin/faq/update";
	}
}
