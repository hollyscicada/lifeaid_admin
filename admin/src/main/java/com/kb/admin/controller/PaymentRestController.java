package com.kb.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kb.admin.domain.PaymentEntity;
import com.kb.api.service.MasterService;
import com.kb.api.util.Common;

@RestController
@RequestMapping(value = "/supervise/admin/v1/payment/**")
public class PaymentRestController {


	@Inject MasterService masterService;
	
	@PostMapping("/search")
	public ResponseEntity<Map<String, Object>> listdashboard(HttpServletRequest request,  Model model, @RequestBody PaymentEntity paymentEntity) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		int record = masterService.dataList("mapper.admin.PaymentMapper", "search", paymentEntity).size();

		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		List<PaymentEntity> list = (List<PaymentEntity>)masterService.dataList("mapper.admin.PaymentMapper", "search", paymentEntity);
		System.out.println(list.size());
		System.out.println(list.size());
		System.out.println(list.size());
		System.out.println(list.size());
		System.out.println(list.size());
		
		map.put("data", list); 

		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
}
