package com.kb.admin.controller;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kb.admin.domain.AdminEntity;
import com.kb.admin.domain.LoginEntity;
import com.kb.api.service.MasterService;
import com.kb.api.util.Common;

@RestController
public class LoginsRestController {
	
	@Inject MasterService masterService;
	
	/*愿�由ъ옄 濡쒓렇�씤*/
	@PostMapping("/api/v1/login/proc")
	public ResponseEntity<Map<String, Object>> login_proc(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, @RequestBody LoginEntity loginEntity) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		int record = masterService.dataCount("mapper.LoginMapper", "adminLogin", loginEntity);
		map.put("success", false);
		if(record > 0) {
			
			AdminEntity adminEntity = (AdminEntity) masterService.dataRead("mapper.LoginMapper", "adminData", loginEntity);
			Object obj=adminEntity;
			for (Field field : obj.getClass().getDeclaredFields()){ 
	            field.setAccessible(true);
	            Object value=field.get(obj);
	            session.setAttribute(field.getName(), value);
	        }
			map.put("success", true);  
		}
	        
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
}
