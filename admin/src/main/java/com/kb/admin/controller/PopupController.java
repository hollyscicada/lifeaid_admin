package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.CSEntity;
import com.kb.admin.domain.PopupEntity;
import com.kb.api.service.MasterService;

@Controller
@RequestMapping(value = "/supervise/popup/**")
public class PopupController {

	private static final Logger logger = LoggerFactory.getLogger(PopupController.class);
	 
	@Inject MasterService masterService;
	
	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<PopupEntity> list =  (List<PopupEntity>) masterService.dataList("mapper.admin.PopupMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/popup/list";
	}
	
	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
	public String PopupDetailGET(Model model , @PathVariable String seq ) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.PopupMapper", "detail", seq));
		return "adminLayout/admin/popup/read";
	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String PopupAddGET(Model model) throws Exception {
		return "adminLayout/admin/popup/create";
	}
	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
	public String PopupUpdateGET(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.PopupMapper", "detail", seq));
		return "adminLayout/admin/popup/update";
	}
}
