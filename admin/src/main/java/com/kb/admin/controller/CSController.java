package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.CSEntity;
import com.kb.api.service.MasterService;


@Controller
@RequestMapping(value = "/supervise/cs/**")
public class CSController {
	private static final Logger logger = LoggerFactory.getLogger(CSController.class);
	 
	@Inject MasterService masterService;
	
	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<CSEntity> list =  (List<CSEntity>) masterService.dataList("mapper.admin.CSMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/cs/list";
	}
	
	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
	public String CSDetailGET(Model model , @PathVariable String seq ) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.CSMapper", "detail", seq));
		return "adminLayout/admin/cs/read";
	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String CSAddGET(Model model) throws Exception {
		return "adminLayout/admin/cs/create";
	}
	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
	public String CSUpdateGET(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.CSMapper", "detail", seq));
		return "adminLayout/admin/cs/update";
	}
	
}
