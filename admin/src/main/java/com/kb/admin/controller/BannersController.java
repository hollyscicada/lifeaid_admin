package com.kb.admin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.BannerEntity;
import com.kb.api.service.MasterService;


@Controller
@RequestMapping(value = "/supervise/banner/*")
public class BannersController {

	private static final Logger logger = LoggerFactory.getLogger(BannersController.class);
	 
	@Inject MasterService masterService;
	 
	
	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String bannerdashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		List<BannerEntity> list =  (List<BannerEntity>) masterService.dataList("mapper.admin.BannerMapper", "list", null);
		model.addAttribute("list", list);
		return "adminLayout/admin/banner/list";
	}
//	@RequestMapping(value = {"/{seq}"} , method = RequestMethod.GET)
//	public String bannerDetailGET(Model model , @PathVariable String seq ) throws Exception {
//		model.addAttribute("detail", masterService.dataRead("mapper.admin.BannerMapper", "detail", seq));
//		return "adminLayout/admin/banner/read";
//	}
	@RequestMapping(value = {"/create"} , method = RequestMethod.GET)
	public String bannerAddGET(Model model) throws Exception {   
		return "adminLayout/admin/banner/create";
	}
	@RequestMapping(value = {"/update/{seq}"} , method = RequestMethod.GET)
	public String bannerUpdateGET(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.admin.BannerMapper", "detail", seq));
		return "adminLayout/admin/banner/update";
	}
}
