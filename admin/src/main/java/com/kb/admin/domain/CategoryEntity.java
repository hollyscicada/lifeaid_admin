package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class CategoryEntity extends CommonEntity{

	
	private String category_seq;
	private String name;
	private String info_seq;
	private String gubun_seq;
	private String sort;
	

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo_seq() {
		return info_seq;
	}

	public void setInfo_seq(String info_seq) {
		this.info_seq = info_seq;
	}

	public String getGubun_seq() {
		return gubun_seq;
	}

	public void setGubun_seq(String gubun_seq) {
		this.gubun_seq = gubun_seq;
	}

	public String getCategory_seq() {
		return category_seq;
	}

	public void setCategory_seq(String category_seq) {
		this.category_seq = category_seq;
	}
	
	
	
}
