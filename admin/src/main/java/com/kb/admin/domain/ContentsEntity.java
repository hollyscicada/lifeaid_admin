package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class ContentsEntity extends CommonEntity{

	private String content_seq;
	private String classify_type;
	private String title;
	private String gubun_seq;
	private String access_code;
	private String thumb_url;
	private String content;
	private String video_type_code;
	private String video_control_code;
	private String file_url;
	private String subscribe_price;
	private String nomal_price;
	private String price;
	private String cancel_content;
	private String img_url;
	
	


	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

	public String getFile_url() {
		return file_url;
	}

	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}

	public String getSubscribe_price() {
		return subscribe_price;
	}

	public void setSubscribe_price(String subscribe_price) {
		this.subscribe_price = subscribe_price;
	}


	public String getNomal_price() {
		return nomal_price;
	}

	public void setNomal_price(String nomal_price) {
		this.nomal_price = nomal_price;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCancel_content() {
		return cancel_content;
	}

	public void setCancel_content(String cancel_content) {
		this.cancel_content = cancel_content;
	}

	public String getAccess_code() {
		return access_code;
	}

	public void setAccess_code(String access_code) {
		this.access_code = access_code;
	}

	public String getThumb_url() {
		return thumb_url;
	}

	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getVideo_type_code() {
		return video_type_code;
	}

	public void setVideo_type_code(String video_type_code) {
		this.video_type_code = video_type_code;
	}

	public String getVideo_control_code() {
		return video_control_code;
	}

	public void setVideo_control_code(String video_control_code) {
		this.video_control_code = video_control_code;
	}

	public String getClassify_type() {
		return classify_type;
	}

	public void setClassify_type(String classify_type) {
		this.classify_type = classify_type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGubun_seq() {
		return gubun_seq;
	}

	public void setGubun_seq(String gubun_seq) {
		this.gubun_seq = gubun_seq;
	}



	public String getContent_seq() {
		return content_seq;
	}

	public void setContent_seq(String content_seq) {
		this.content_seq = content_seq;
	}
}
