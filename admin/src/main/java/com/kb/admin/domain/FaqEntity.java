package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class FaqEntity extends CommonEntity {
	int faq_seq;
	String title; 
	String content;
	String faq_code;
	String answer;
	
	
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public int getFaq_seq() {
		
		return faq_seq;
	}
	public String getFaq_code() {
		return faq_code;
	}
	public void setFaq_code(String faq_code) {
		this.faq_code = faq_code;
	}

	public void setFaq_seq(int faq_seq) {
		this.faq_seq = faq_seq;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	} 
	
	
	
	
	
}
