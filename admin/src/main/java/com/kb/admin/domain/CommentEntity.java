package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class CommentEntity extends CommonEntity {
	
	private String comment_seq;
	private String member_seq;
	private String content_seq;
	private String content;
	private String premium_yn;
	
	
	public String getComment_seq() {
		return comment_seq;
	}
	public void setComment_seq(String comment_seq) {
		this.comment_seq = comment_seq;
	}
	public String getMember_seq() {
		return member_seq;
	}
	public void setMember_seq(String member_seq) {
		this.member_seq = member_seq;
	}
	public String getContent_seq() {
		return content_seq;
	}
	public void setContent_seq(String content_seq) {
		this.content_seq = content_seq;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPremium_yn() {
		return premium_yn;
	}
	public void setPremium_yn(String premium_yn) {
		this.premium_yn = premium_yn;
	}
	
	

}
