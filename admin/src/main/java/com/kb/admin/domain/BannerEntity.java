package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class BannerEntity extends CommonEntity {
	private String adzone_seq;
	private String category_seq;
	private String location;
	private String content;
	private String sort;
	private String link_url;
	private String img_url;
	
	
	
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getAdzone_seq() {
		return adzone_seq;
	}
	public void setAdzone_seq(String adzone_seq) {
		this.adzone_seq = adzone_seq;
	}
	public String getCategory_seq() {
		return category_seq;
	}
	public void setCategory_seq(String category_seq) {
		this.category_seq = category_seq;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getLink_url() {
		return link_url;
	}
	public void setLink_url(String link_url) {
		this.link_url = link_url;
	}
	
	
	
	
	
}
