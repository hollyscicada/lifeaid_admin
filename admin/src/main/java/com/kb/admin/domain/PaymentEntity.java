package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class PaymentEntity extends CommonEntity{

	private String payment_seq;
	private String payment_num;
	private String member_seq;
	private String price;
	private String payment_type;
	private String status_code;
	private String product_code;
	private String content_seq;
	private String tot_price;
	private String pg_code;
	private String premium_start_dt;
	
	
	

	public String getPremium_start_dt() {
		return premium_start_dt;
	}
	public void setPremium_start_dt(String premium_start_dt) {
		this.premium_start_dt = premium_start_dt;
	}
	public String getPg_code() {
		return pg_code;
	}
	public void setPg_code(String pg_code) {
		this.pg_code = pg_code;
	}
	private String getTot_price() {
		return tot_price;
	}
	private void setTot_price(String tot_price) {
		this.tot_price = tot_price;
	}
	public String getPayment_seq() {
		return payment_seq;
	}
	public void setPayment_seq(String payment_seq) {
		this.payment_seq = payment_seq;
	}
	public String getPayment_num() {
		return payment_num;
	}
	public void setPayment_num(String payment_num) {
		this.payment_num = payment_num;
	}
	public String getMember_seq() {
		return member_seq;
	}
	public void setMember_seq(String member_seq) {
		this.member_seq = member_seq;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	public String getStatus_code() {
		return status_code;
	}
	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getContent_seq() {
		return content_seq;
	}
	public void setContent_seq(String content_seq) {
		this.content_seq = content_seq;
	}
	
	
}
