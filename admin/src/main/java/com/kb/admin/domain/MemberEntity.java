package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class MemberEntity extends CommonEntity{

	private String admin_seq;
	private String member_seq;
	private String member_id;
	private String password;
	private String phone;
	private String category_seq;
	private String premium_seq;
	private String premium_yn;
	private String premium_start_dt;
	private String premium_end_dt;
	private String login_history;
	private String premium_name;
	
	
	
	public String getAdmin_seq() {
		return admin_seq;
	}
	public void setAdmin_seq(String admin_seq) {
		this.admin_seq = admin_seq;
	}
	public String getPremium_name() {
		return premium_name;
	}
	public void setPremium_name(String premium_name) {
		this.premium_name = premium_name;
	}
	public String getLogin_history() {
		return login_history;
	}
	public void setLogin_history(String login_history) {
		this.login_history = login_history;
	}
	public String getPremium_start_dt() {
		return premium_start_dt;
	}
	public void setPremium_start_dt(String premium_start_dt) {
		this.premium_start_dt = premium_start_dt;
	}
	public String getPremium_end_dt() {
		return premium_end_dt;
	}
	public void setPremium_end_dt(String premium_end_dt) {
		this.premium_end_dt = premium_end_dt;
	}
	public String getPremium_yn() {
		return premium_yn;
	}
	public void setPremium_yn(String premium_yn) {
		this.premium_yn = premium_yn;
	}
	public String getMember_seq() {
		return member_seq;
	}
	public void setMember_seq(String member_seq) {
		this.member_seq = member_seq;
	}
	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCategory_seq() {
		return category_seq;
	}
	public void setCategory_seq(String category_seq) {
		this.category_seq = category_seq;
	}
	public String getPremium_seq() {
		return premium_seq;
	}
	public void setPremium_seq(String premium_seq) {
		this.premium_seq = premium_seq;
	}
	
	
	
}