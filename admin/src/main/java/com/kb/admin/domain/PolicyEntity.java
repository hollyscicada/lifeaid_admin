package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class PolicyEntity extends CommonEntity {

	
	private String policy_seq;;
	private String title;
	private String content;
	
	
	public String getPolicy_seq() {
		return policy_seq;
	}
	public void setPolicy_seq(String policy_seq) {
		this.policy_seq = policy_seq;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
	
}
