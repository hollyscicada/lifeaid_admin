package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class LoginEntity extends CommonEntity{
	private String admin_seq;
	private String password;
	private String name;
	private String admin_id;
	
	public String getAdmin_seq() {
		return admin_seq;
	}
	public void setAdmin_seq(String admin_seq) {
		this.admin_seq = admin_seq;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) { 
		this.name = name;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	
	
	
	
	
	
	
	
}
