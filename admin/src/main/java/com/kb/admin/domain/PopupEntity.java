package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class PopupEntity extends CommonEntity{

	private String popup_seq;
	private String title;
	private String device_code;
	private String x_location;
	private String y_location;
	private String start_dt;
	private String end_dt;
	private String p_width;
	private String p_height;
	
	
	public String getPopup_seq() {
		return popup_seq;
	}
	public void setPopup_seq(String popup_seq) {
		this.popup_seq = popup_seq;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDevice_code() {
		return device_code;
	}
	public void setDevice_code(String device_code) {
		this.device_code = device_code;
	}
	public String getX_location() {
		return x_location;
	}
	public void setX_location(String x_location) {
		this.x_location = x_location;
	}
	public String getY_location() {
		return y_location;
	}
	public void setY_location(String y_location) {
		this.y_location = y_location;
	}
	public String getStart_dt() {
		return start_dt;
	}
	public void setStart_dt(String start_dt) {
		this.start_dt = start_dt;
	}
	public String getEnd_dt() {
		return end_dt;
	}
	public void setEnd_dt(String end_dt) {
		this.end_dt = end_dt;
	}
	public String getP_width() {
		return p_width;
	}
	public void setP_width(String p_width) {
		this.p_width = p_width;
	}
	public String getP_height() {
		return p_height;
	}
	public void setP_height(String p_height) {
		this.p_height = p_height;
	}
	
	
}
