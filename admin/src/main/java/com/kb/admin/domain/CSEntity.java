package com.kb.admin.domain;

import com.kb.api.domain.CommonEntity;

public class CSEntity extends CommonEntity{

	private String cs_seq;
	private String cs_code;
	private String member_seq;
	private String title;
	private String cs_yn;
	private String content;
	private String answer;
	private String answer_by;
	
	
	
	
	public String getContent() {
		return content;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getAnswer_by() {
		return answer_by;
	}
	public void setAnswer_by(String answer_by) {
		this.answer_by = answer_by;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCs_yn() {
		return cs_yn;
	}
	public void setCs_yn(String cs_yn) {
		this.cs_yn = cs_yn;
	}
	public String getCs_seq() {
		return cs_seq;
	}
	public void setCs_seq(String cs_seq) {
		this.cs_seq = cs_seq;
	}
	public String getCs_code() {
		return cs_code;
	}
	public void setCs_code(String cs_code) {
		this.cs_code = cs_code;
	}
	public String getMember_seq() {
		return member_seq;
	}
	public void setMember_seq(String member_seq) {
		this.member_seq = member_seq;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
}
