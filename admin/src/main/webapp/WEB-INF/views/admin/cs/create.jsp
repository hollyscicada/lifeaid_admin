<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	$(function(){
		$(".submit-btn").click(function(){
			var admin_id = $("input[name=admin_id]");
			var password = $("input[name=password]");
			var name = $("input[name=name]");
			var write_by = $("input[name=write_by]");
			
			var param = {
					"admin_id" : admin_id.val(),
					"password" : password.val(),
					"name" : name.val(),
					"write_by": write_by.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/admin/create", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/admin/list"
				}
			},function(){})
		})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					<div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">답변 추가</h1>
                                </div>
                            </div>
						<form>
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div class="row">
								<div style="margin-left:30px; margin-right:30px;">
										<textarea rows="30" cols="100" name="cs_content">
										답변을 등록해주세요
										</textarea>
								</div>
							</div>
						</form>
							<div style="float:right;">
								<button type="button" class="btn btn-primary waves-effect waves-light submit-btn">저장</button>
								<button type="button" class="btn btn-primary waves-effect waves-light cancel-btn">취소</button>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Static Table End -->
