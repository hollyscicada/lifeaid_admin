<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
			$(".btn-success").click(function(){
				if(confirm("정말 수정하시겠습니까?")){
					var seq = "${detail.cs_seq }";
					location.href="/supervise/admin/v1/cs/update/"+seq;
				}
			})
			$(".btn-danger").click(function(){
				if(confirm("정말 삭제하시겠습니까?")){
					var seq = "${detail.cs_seq }";
					ajaxCallGet("/supervise/admin/v1/cs/remove?seq="+seq, function(res){
						if(res.success){
							alert("작업이 완료되었습니다.");
							location.href="/supervise/cs/list"
						}
					})
				}
			})
			$(".btn-list").click(function(){
					location.href="/supervise/cs/list"
			
			})
			$(".btn-reply").click(function(){
		         $('.popup_cs').fadeIn();
		    })

	})

	function answer_put(){
			var answer = $("input[name=answer]");
			var answer_by = $("input[name=answer_by]");
			
			var param = {
					"answer" : answer.val(),
					"answer_by" : answer_by.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/cs/create", param, function(res){
				if(res.success){
					var ss = "";
				for(var i = 0; i < res.data.length ; i++){

					  ss +=  `<tr>
	                       <td>`+res.data[i].answer+`</td>
	                       <td>`+res.data[i].answer_by+`</td>
	                       <td>${phone }</td>
	                   </tr>`;
					   $(".tbody").html(ss)
				}
					alert("답변 등록이 완료되었습니다.");
					location.href="/supervise/cs/list"

				}
			},function(){})

	}

</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
						<div class="sparkline13-hd">
                               <div style="padding: 0 50px 0 0">
                                    <h1>C/S 1:1 문의 상세/답변</h1>
                              </div>
                   	    </div>
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th>문의유형</th>
			                                        <td class="form-control">${detail.cs_code }</td>
													<th>등록일</th>
			                                        <td class="form-control">${detail.create_dt_yyyymmdd }</td>
												</tr>
			                                    <tr>
													<th>문의제목</th>
			                                        <td class="form-control">${detail.title }</td>
			                                    </tr>
			                                    <tr>
													<th>작성자</th>
			                                        <td class="form-control">${detail.member_seq }</td>
													<th>처리상태</th>
			                                        <td class="form-control">${detail.cs_yn }</td>
			                                    </tr>
			                                    <tr>
													<th>문의내용</th>
			                                        <td class="form-control">${detail.content }</td>
			                                    </tr>    
			                                    <tr >
													<th>답변내용</th>
													<td class="form-control">${detail.answer }
														(작성자 : <span>${detail.answer_by}</span>)
														<button type="button" class="btn btn-success">수정</button>
														<button type="button" class="btn btn-danger">삭제</button>
													</td>
													
			                                    </tr>
			                                </tbody>
			                            </table>
									</div>
									
									



								</div>
							</form>
						</div>
						
							<button type="button" class="btn btn-custon-four btn-reply" style="float:right;margin-left:10px">답변</button>
							<button type="button" class="btn btn-custon-four btn-list"style="float:right;">목록</button>
								
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
    <div class="popup-bg"></div>
        <div class="popup position-center popup_cs">
          <p class="font-20r font-xs-16r">답변 추가</p>
		  <hr/>
		  	<form>
			<div>
				<input type="hidden" name="answer_by" value="${sessionScope.admin_seq}">
				<textarea rows="20" cols="100" name="answer">답변을 등록해주세요</textarea>
			</div>
				<input type="button" value="저장" class="popup-btn" onclick="answer_put()">
				<input type="button" value="취소" class="popup-btn" onclick="">
			</form>
        </div>
