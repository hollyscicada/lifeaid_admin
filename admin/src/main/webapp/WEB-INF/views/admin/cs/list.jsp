<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
    
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(){
    			location.href="/supervise/cs/"+$(this).attr("data-seq")
    		})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
			$( ".start" ).datepicker({
		        showOn: "both", 
		        buttonImage: "/resources/images/sub/icon_calender.jpg", 
		        buttonImageOnly: true ,
			dateFormat	: "yy-mm-dd",
		        
		    });
		    $( ".finish" ).datepicker({
		        showOn: "both", 
		        buttonImage: "/resources/images/sub/icon_calender.jpg", 
		        buttonImageOnly: true ,
		        dateFormat	: "yy-mm-dd"
		    });
    	})
    	
       	$(function(){
		$(".btn-success").click(function(){
			var start_1 = $("input[name=start_1]");
			var finish_1 = $("input[name=finish_1]");
			var start_2 = $("input[name=start_2]");
			var finish_2 = $("input[name=finish_2]");

			
			var category_1 = $("input[name=category_1]");
			var category_2 = $("input[name=category_2]");
			var category_3 = $("input[name=category_3]");
			var category_4 = $("input[name=category_4]");
			var keyword_1 = $("input[name=keyword_1]");
			var keyword_2 = $("input[name=keyword_2]");
			var member_state = $("input[name=member_state]");
			var phone = $("input[name=phone]");
			var premium_seq = $("input[name=premium_seq]");
			var category_seq = $("input[name=category_seq]");

			
			var param = {
					"member_id" : member_id.val(),
					"password" : password.val(),
					"name" : name.val(),
					"write_by": write_by.val(),
					"sex" : sex.val(),
					"yyyy" : yyyy.val(),
					"email" : email.val(),
					"phone" : phone.val(),
					"premium_seq" : premium_seq.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/member/create", param, function(res){
				if(res.success){
					alert("회원 등록이 완료되었습니다.");
					location.href="/supervise/member/list"

					
				$(".listlength").html(res.data.length)
				}
			},function(){})
			})
		})
    </script>
    
        <!-- Static Table Start -->
    <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>C/S 1:1 문의</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                    <div class="row">
                                    	<div>
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
            										
            									<th>문의유형</th>
            									<td colspan="3">
            									<select id="search_type" class="form-control" >
            										<option value="all">전체</option>
            										<option value="y">정상</option>
            										<option value="n">탈퇴</option>
            									</select>
            									</td>
            									<th>검색조건</th>
            									<td colspan="3">
            									<select id="search_type" class="form-control" >
            										<option value="all">전체</option>
            										<option value="y">정상</option>
            										<option value="n">탈퇴</option>
            									</select>
            									</td>
            								</tr>
            								<tr>		
            									<th>검색 기간</th>
            									<td >
            										<input type="date" class="datepicker form-control-sm" name="start_1"/>
            										<a href="#"></a> ~ 
            										<input type="date" class="datepicker form-control-sm" name="finish_1"/>
												</td>
            									<th>검색어 입력</th>
		            							<td colspan="6">			
		            								<input type="text" id="keyword_2" class="form-control" >
		            							</td>
            								</tr>
            							</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px">검색</button>
										<button type="reset" class="btn btn-primary btn-success" style="float:right;">초기화</button>
			
                               </form>    
                               </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px">
                                    	<input type="hidden" value="${member.member_seq}">
										<h4 >조회 건수 : <span class="listlength">${fn:length(list)}</span> 건</h4>
							        </div>
                                    <div class="row">
                                   
                                     <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                               <thead>
                                            <tr>
                                            	<th>번호</th>
                                            	<th>문의유형</th>
                                            	<th>제목</th>
                                            	<th>작성자</th>
                                            	<th>등록일</th>
                                            	<th>처리상태</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${list }" var="item" >
                                           	   <tr class="go-detail-item" data-seq="${item.cs_seq}">
	                                                <td>${item.cs_seq }</td>
	                                                <td>${item.cs_code }</td>
	                                                <td>${item.title }</td>
	                                                <td>${item.member_seq }</td>
	                                                <td>${item.create_dt_yyyymmdd }</td>
	                                                <td>${item.cs_yn }</td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    
                                    </div>

                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Static Table End -->
