<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	$(function(){
		$(".submit-btn").click(function(){
			var admin_id = $("input[name=admin_id]");
			var password = $("input[name=password]");
			var name = $("input[name=name]");
			var write_by = $("input[name=write_by]");
			
			var param = {
					"admin_seq" : "${detail.admin_seq}",
					"admin_id" : admin_id.val(),
					"password" : password.val(),
					"name" : name.val(),
					"write_by": write_by.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/admin/update", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/admin/list"
				}
			},function(){})
		})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>관리자 <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input name="admin_id" type="text" class="form-control" value="${detail.admin_id }"
												placeholder="아이디">
										</div>
										<div class="form-group">
											<input name="password" type="password" class="form-control"
												placeholder="비밀번호(수정하실경우에만 작성해주세요.)">
										</div>
										<div class="form-group">
											<input name="name" type="text" class="form-control"
												placeholder="이름" value="${detail.name }">
										</div>
									</div>



								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="payment-adress">
											<button type="button"
												class="btn btn-primary waves-effect waves-light submit-btn">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
