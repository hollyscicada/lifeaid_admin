<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
			$(".btn-success").click(function(){
				if(confirm("정말 수정하시겠습니까?")){
					var seq = "${detail.admin_seq }";
					location.href="/supervise/admin/v1/admin/update/"+seq;
				}
			})
			$(".btn-danger").click(function(){
				if(confirm("정말 삭제하시겠습니까?")){
					var seq = "${detail.admin_seq }";
					ajaxCallGet("/supervise/admin/v1/admin/remove?seq="+seq, function(res){
						if(res.success){
							alert("작업이 완료되었습니다.");
							location.href="/supervise/admin/list"
						}
					})
				}
			})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>관리자 <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th>관리자 정보</th>
													<th>결과</th>
												</tr>
			                                    <tr>
			                                        <td>아이디</td>
			                                        <td>${detail.admin_id }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>이름</td>
			                                         <td>${detail.name }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>생성일자</td>
			                                        <td>${detail.create_dt_yyyymmdd }</td>
			                                    </tr>
			                                </tbody>
			                            </table>
									</div>
									
									



								</div>
							</form>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						
						<button type="button" class="btn btn-custon-four btn-danger" style="float:right;margin-left:10px">삭제</button>
						<button type="button" class="btn btn-custon-four btn-success"style="float:right;">수정</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
