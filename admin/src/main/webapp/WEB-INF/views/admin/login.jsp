<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<script>
		$(function(){

			$(".loginbtn").click(function(){

				var admin_id = $("#admin_id").val();
				var password = $("#password").val();
				
				var param = {
						"admin_id" : admin_id,
						"password" : password
				}
				
				ajaxCallPost("/api/v1/login/proc", param, function(res){
					if(res.success){
						location.href="/";
					}else{
						alert("아이디 또는 비밀번호를 확인해주세요.")
						
					}
				},function(){})
			})
		})
	</script>
     <div class="all-content-wrapper" style="min-height: 900px;background: white; ">
        
	<div class="col-lg-12 col-md-12" style="text-align:center;">
			<div class="">
				<h3>Administrator System</h3>
			
			</div>
			
		<div class="row">
			<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" >
			
				<h3>로그인</h3>
				<p>아이디를 정확하게 입력해주세요.</p>
				<p>개인정보 보호를 위하여 비밀번호는 6자이상 12자이하로 구성되어 있습니다.</p>
				<p>(영문 대소문자, 숫자, 특수문자 중 2가지 혼합)</p>

			<div>
			<textarea rows="10" cols="10">광고창</textarea></div>
			<div class="button_box">
				<button type="button" class="ios_btn">앱스토어에서 받기</button>
                <button type="button" class="android_btn">플레이스토어에서 받기</button>
            </div>
           </div>
			<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
				<div class="">
                    <div class="">
                        <form action="#" id="loginForm" name="loginForm">
                            <div class="form-group">
                                <label class="control-label" for="username">ID</label>
                                <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="아이디를 입력해주세요" value="" name="admin_id" id="admin_id" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******" required="비밀번호를 입력해주세요" value="" name="password" id="password" class="form-control">
                            </div>
                            <button type="button" class="btn btn-success btn-block loginbtn">Login</button>
                        </form>
                    </div>
                </div>
			</div>

		</div>   
		
			<div class="text-center login-footer">
				<p>Copyright © weplanner. All rights reserved. Template by </p>
			</div>
    </div>
  
    </div>