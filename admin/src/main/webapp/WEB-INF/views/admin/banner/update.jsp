<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	$(function(){
		$(document).on("click", ".submit-btn", function(){
				var adzone_seq = $(this).attr("data-seq");
				var img_url = $("input[name=img_url]");
			//	var category_seq = $("#category_seq").val();
				var location = $("input[name=location]");
				var content = $("input[name=content]");
				var link_url = $("input[name=link_url]");
				var write_by = $("input[name=write_by]");
				var show_yn = $("input[name=show_yn]:checked");
				var param = {
					//	"sort" : sort.val(),
						"adzone_seq" : adzone_seq,
						"img_url" : img_url.val(),
						"location" : location.val(),
						"content" : content.val(),
						"link_url" : link_url.val(),
						"show_yn" : show_yn.val(),
						"write_by": write_by.val()
				}
				
				ajaxCallPost("/supervise/api/v1/banner/update", param, function(res){
					if(res.success){
						alert("배너가 수정되었습니다.");
						location.href="/supervise/banner/list";
					}
				},function(){})
			})
		/*
			 파일업로드
		*/
		$(".img_fun").change(function(){
			ajaxUpload($(".img_fun")[0].files[0], "adzone", function(data_url) {
				$("input[name='img_url']").val(data_url);
			})
		})

		    $(document).on("click", ".cancel-btn", function(){
				location.href="/supervise/banner/list";
			})
		
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
							<div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">광고 추가 / 수정</h1>
                                </div>
                            </div>
                            </div>
											<form>
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div class="row">
								<div style="margin-left:30px; margin-right:30px;">
										<table id="user" class="table table-bordered x-editor-custom">
			                       
			                                <tbody>
			                                	<tr>
													<th>관리자</th>
													<td><input name="title" type="text" class="form-control" value="${detail.create_by_nm}"></td>
												</tr>
			                                    <tr>
			                                        <th>제목(내용)</th>
													<td>
														<input name="content" type="text" class="form-control" value="${detail.content}">
													</td>
												</tr>
			                                   <tr>	
													<th>광고 위치</th>
													<td>
													<select name="location" class="form-control">
														<option value="">광고 위치</option>
													</select>
												  	</td>
						                       </tr> 
						                       <tr>	
													<th>카테고리</th>
													<td>
													<select id="category_seq" name="category_seq" class="form-control">
														<option value="">없음</option>
													</select>
												  	</td>
						                       </tr> 
				                               <tr>   
			                                        <th>이미지</th>
													<td>
														${detail.img_url }
														<input name="img_url" type="hidden" class="form-control" value="${detail.img_url }">
														<input type="file" class="form-control img_fun" >
													</td>
												</tr>
			                                    <tr>
													<th>링크</th>
													<td>
														<input name="link_url" type="text" class="form-control" value="${detail.link_url}">
													</td>
			                                    </tr>
			                                    <tr>
			                                        <th>상태</th>
													<td>
														<input name="show_yn" type="radio" value="Y" checked>
														<span>게시</span>
														<input name="show_yn" type="radio" value="N">
														<span>게시 안함</span>
													</td>
												</tr>
			                                   
			                                </tbody>
			                            </table>
								</div>
							</div>
						</form>
							<div style="float:right;">
								<button type="button" class="btn btn-primary waves-effect waves-light submit-btn" data-seq="${detail.adzone_seq}">저장</button>
								<button type="button" class="btn btn-primary waves-effect waves-light cancel-btn">취소</button>
							</div>		

					</div>
				</div>
			</div>
		</div>
	</div>
</div>