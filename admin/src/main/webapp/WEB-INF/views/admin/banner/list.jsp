<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
<c:set var="name" value="adzone" />
 <c:set var="seq" value="adzone_seq" /> 
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(e){
	    			location.href="/supervise/banner/update/"+$(this).attr("data-seq")
    		})
    		$(document).on("click", ".btn-success", function(){
				location.href="/supervise/banner/create";
			})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
    	})
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>배너관리</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
           
                                    <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th>번호</th>
                                                <th>관리자</th>
                                                <th>내용</th>
                                                <th>광고위치</th>
                                                <th>노출여부</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${list }" var="item" >
                                           		<c:set var="temp_seq" value="${item.adzone_seq }" />
                                           	   <tr>
	                                                <td>${item.rownum }</td>
	                                                <td class="go-detail-item" data-seq="${item.adzone_seq}">${item.create_by_nm }</td>
	                                                <td>${item.content }</td>
	                                                <td>${item.location }</td>
	                                                <%@ include file="/WEB-INF/views/admin/common/showYn.jsp" %>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <button type="button" class="btn btn-custon-four btn-success" style="float: right;margin-top: 30px;">+추가하기</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Static Table End -->
