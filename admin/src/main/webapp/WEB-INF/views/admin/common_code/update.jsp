<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	$(function(){
		$(document).on("click", ".submit-btn", function(){
			var common_seq = "${detail.common_seq}";
			var name = $("input[name=name]");
			var code = $("input[name=code]");
			var sub_code = $("input[name=sub_code]");
			var discription = $("input[name=discription]");
			var write_by = $("input[name=write_by]");
			var param = {
					"common_seq":common_seq,
					"name" : name.val(),
					"code" : code.val(),
					"sub_code" : sub_code.val(),
					"discription" : discription.val(),
					"write_by": write_by.val()
			}
			
			ajaxCallPost("/supervise/api/v1/common/update", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/common/list"
				}
			},function(){})
		})
		
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>FAQ <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input name="name" type="text" class="form-control" placeholder="코드명" value="${detail.name }">
										</div>
										<div class="form-group">
											<input name="code" type="text" class="form-control" placeholder="대분류코드" value="${detail.code }">
										</div>
										<div class="form-group">
											<input name="sub_code" type="text" class="form-control" placeholder="소분류코드" value="${detail.sub_code }">
										</div>
										<div class="form-group">
											<input name="discription" type="text" class="form-control" placeholder="설명" value="${detail.discription }">
										</div>
										<div class="form-group">
											<input name="sort" type="number" class="form-control" placeholder="순서" value="${detail.sort }">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="payment-adress">
											<button type="button"
												class="btn btn-primary waves-effect waves-light submit-btn" >Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
<%@include file="/WEB-INF/views/admin/common/edit_form.jsp"%>