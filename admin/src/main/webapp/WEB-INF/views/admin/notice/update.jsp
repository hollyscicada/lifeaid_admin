<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	function goSubmit() {
		submitContents("submit-btn", "content");
	}
	$(function(){
		$(document).on("click", ".submit-btn", function(){
			var notice_seq = "${detail.notice_seq}";
			var title = $("input[name=title]");
			var content = $("textarea[name=content]");
			var file_url = $("input[name=file_url]");
			var write_by = $("input[name=write_by]");
			
			
			var param = {
					"notice_seq":notice_seq,
					"title" : title.val(),
					"content" : content.val(),
					"file_url" : file_url.val(),
					"write_by": write_by.val()
			}
			
			ajaxCallPost("/supervise/api/v1/notice/update", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/notice/list"
				}
			},function(){})
		})
		
		/*
			 파일업로드
		*/
		$(".file_fun").change(function(){
			ajaxUpload($(".file_fun")[0].files[0], "notice", function(data_url) {
				$("input[name='file_url']").val(data_url);
				$("#file_txt").text(data_url);
			})
		})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>공지사항 <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input name="title" type="text" class="form-control" placeholder="제목" value="${detail.title }">
										</div>
										<div class="form-group">
											<textarea id="ir1" rows="10" cols="300" name="content">${detail.content }</textarea>
										</div>
										<div class="form-group">
											<span id="file_txt">${detail.file_url }</span>
											<input name="file_url" type="hidden" class="form-control" value="${detail.file_url }">
											<input name="" type="file" class="form-control file_fun" >
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="payment-adress">
											<button type="button"
												class="btn btn-primary waves-effect waves-light" onclick="javascript:goSubmit()">Submit</button>
												<button class="submit-btn" type="submit" style="display: none"></button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
<%@include file="/WEB-INF/views/admin/common/edit_form.jsp"%>