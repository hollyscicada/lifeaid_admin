<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
 <c:set var="name" value="notice" />
 <c:set var="seq" value="notice_seq" />   
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(e){
    			if(!is_show){
    				location.href="/supervise/${name}/"+$(this).attr("data-seq")
    			}
    		})
    		$(document).on("click", ".btn-success", function(){
				location.href="/supervise/${name}/create";
			})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
    	})
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>공지사항 <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control dt-tb">
											<option value="">Export Basic</option>
											<option value="all">Export All</option>
											<option value="selected">Export Selected</option>
										</select>
                                    </div>
                                    <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th>번호</th>
                                                <th>제목</th>
                                                <th>조회수</th>
                                                <th>생성일자</th>
                                                <th>작성자</th>
                                                 <th>노출여부</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${list }" var="item" >
                                            	<c:set var="temp_seq" value="${item.notice_seq }" />
                                           	   <tr class="go-detail-item" data-seq="${item.notice_seq}">
	                                                <td>${item.rownum }</td>
	                                                <td>${item.title }</td>
	                                                <td>${item.view_cnt }</td>
	                                                <td>${item.create_dt_yyyymmdd }</td>
	                                                <td>${item.create_by_nm }</td>
	                                              	<%@ include file="/WEB-INF/views/admin/common/showYn.jsp" %>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <button type="button" class="btn btn-custon-four btn-success" style="float: right;margin-top: 30px;">등록</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Static Table End -->
