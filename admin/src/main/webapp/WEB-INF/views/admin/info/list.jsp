<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
    
  
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(){
    			location.href="/supervise/payment/"+$(this).attr("data-seq")
    		})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
			setTimeout(function(){
				$("#select").fadeIn(1000);
			},500)
	    
			$( ".start" ).datepicker({
		        showOn: "both", 
		        buttonImage: "/resources/images/sub/icon_calender.jpg", 
		        buttonImageOnly: true ,
			dateFormat	: "yy-mm-dd",
		        
		    });
		    $( ".finish" ).datepicker({
		        showOn: "both", 
		        buttonImage: "/resources/images/sub/icon_calender.jpg", 
		        buttonImageOnly: true ,
		        dateFormat	: "yy-mm-dd"
		    });

	
    	})
    	
    	$(function(){
		$(".btn-success").click(function(){
			var start_1 = $("input[name=start_1]");
			var finish_1 = $("input[name=finish_1]");
			var start_2 = $("input[name=start_2]");
			var finish_2 = $("input[name=finish_2]");

			
			var category_1 = $("input[name=category_1]");
			var category_2 = $("input[name=category_2]");
			var category_3 = $("input[name=category_3]");
			var category_4 = $("input[name=category_4]");
			var keyword_1 = $("input[name=keyword_1]");
			var keyword_2 = $("input[name=keyword_2]");
			var member_state = $("input[name=member_state]");
			var phone = $("input[name=phone]");
			var premium_seq = $("input[name=premium_seq]");
			var category_seq = $("input[name=category_seq]");

			
			var param = {
					"member_id" : member_id.val(),
					"password" : password.val(),
					"name" : name.val(),
					"write_by": write_by.val(),
					"sex" : sex.val(),
					"yyyy" : yyyy.val(),
					"email" : email.val(),
					"phone" : phone.val(),
					"premium_seq" : premium_seq.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/member/create", param, function(res){
				if(res.success){
					alert("회원 등록이 완료되었습니다.");
					location.href="/supervise/member/list"
				}
			},function(){})
		})
	})
	


		
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>회사소개</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                    <div class="row">
                                    	<div>
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
            									<th>회사소개<br>
            									</th>
            									<td>
            										<textarea name="content" rows="100" cols="100" class="form-control"></textarea>
												</td>
            								</tr>
            								<tr>		
            									<th>개인정보 수집 및 이용<br>
            										(회원가입시)
            									</th>
              									<td>
            										<textarea name="content" rows="100" cols="50" class="form-control"></textarea>
												</td>
            								</tr>
            								<tr>		
            									<th>개인정보처리방침</th>
            									<td>
            										<textarea name="content" rows="100" cols="50" class="form-control"></textarea>
												</td>
											</tr>
											<tr>
            									<th>개인정보 제3자 제공 동의</th>
            	            					<td>
            										<textarea name="content" rows="100" cols="50" class="form-control"></textarea>
												</td>
            					
            								</tr>
      								</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px">저장</button>
			
                               </form>    
                               </div>
                                
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       </div>
        <!-- Static Table End -->
