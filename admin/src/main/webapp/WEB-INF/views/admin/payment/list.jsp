<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
    
  
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(){
    			location.href="/supervise/payment/"+$(this).attr("data-seq")
    		})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
			setTimeout(function(){
				$("#select").fadeIn(1000);
			},500)
	    

		});

	
    	function DataSearch(){
			var start_1 = $("input[name=start_1]");
			var finish_1 = $("input[name=finish_1]");
			var premium_seq = $("input[name=premium_seq]");
			var payment_type = $("#payment_type").val();
			var status_code = $("#status_code").val();
			var search_type = $("#search_type").val();
			var search_value = $("input[name=search_value]");
			
			var param = {
					"start_1" : start_1.val(),
					"finish_1" : finish_1.val(),
					"premium_seq" : premium_seq.val(),
					"payment_type": payment_type,
					"status_code" : status_code,
					"search_type" : search_type,
					"search_value" : search_value.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/payment/search", param, function(res){
				if(res.success){
					var ss = "";
					for(var i = 0; i < res.data.length ; i++){

					  ss +=  `<tr>
	                       <td>`+res.data[i].payment_seq+`</td>
	                       <td>`+res.data[i].payment_num+`</td>
	                       <td>`+res.data[i].name+`"/></td>
						   <td>`+res.data[i].price+`</td>
	                       <td>`+res.data[i].status_code+`</td>
	                       <td>`+res.data[i].product_code+`</td>
	                       <td>`+res.data[i].content_seq+`</td>
						   <td class="go-detail-item" data-seq="`+res.data[i].content_seq+`">`+res.data[i].content_seq+`</td>
	                   </tr>`;
					$(".tbody").html(ss)
					
					$(".listlength").html(res.data.length)
					}
					alert("조회되었습니다.");
					//location.href="/supervise/member/list"
				}
			},function(){})
		};

	
		
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>결제 리스트</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                    <div class="row">
                                    	<div>
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
            									<th>결제구분</th>
            									<td>
            										<input type="radio" class="form-control-sm" name="premium_seq" value="1">
            										<span>전체회원</span>
            										<input type="radio" class="form-control-sm" name="premium_seq" value="2">
            										<span>일반회원</span>
            										<input type="radio" class="form-control-sm" name="premium_seq" value="3">
													<span>프리미엄회원</span>	

												</td>
            										
            									<th>결제수단</th>
            									<td>
            									<select id="payment_type" class="form-control" >
            										<option value="">전체</option>
            										<option value="y">카드</option>
            										<option value="n">무통장입금</option>
            									</select>
            									</td>
            								</tr>
            								<tr>		
            								<th>결제일자</th>
            									<td >
            										<input type="date" class="datepicker form-control-sm" name="start_1"/>
            										<a href="#"></a> ~ 
            										<input type="date" class="datepicker form-control-sm" name="finish_1"/>
												</td>
            								<th>결제상태</th>
            									<td>
                								<select id="status_code" class="form-control" >
            										<option value="0">주문접수</option>
            										<option value="1">주문완료</option>
            										<option value="2">입금/결제</option>
            										<option value="3">파일 다운로드</option>
            										<option value="4">구매확정</option>
            										<option value="5">환불</option>
            										<option value="6">환불완료</option>
            									</select>
												</td>
            					
            								</tr>
      
            								<tr>
            									<th>검색</th>
            									<td colspan="6">		            									
            											<select id="search_type" class="form-control">
		            										<option value="id">회원 ID</option>
		            										<option value="name">회원명</option>
		            										<option value="payment_num">주문번호</option>
		            										<option value="pg_code">승인번호</option>
		            										<option value="product_code">상품명</option>
		            									</select>
		            										
		            								<input type="text" name="search_value" class="form-control" >
		            							</td>
            								</tr>
            							</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px" onclick="DataSearch()">조회</button>
										<button type="reset" class="btn btn-primary btn-success" style="float:right;">초기화</button>
			
                               </form>    
                               </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px">
										<h4 >조회 건수 : <span class="listlength">${fn:length(list)}</span> 건</h4>
							        </div>
                                    <div class="row">
                                   
                                     <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                               <thead>
                                            <tr>
                                            	<th>No</th>
                                            	<th>주문번호</th>
                                            	<th>회원명</th>
                                            	<th>결제금액</th>
                                            	<th>결제수단</th>
                                            	<th>결제상태</th>
                                            	<th>상품구분</th>
                                              	<th>상품상세</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody">
                                            <c:forEach items="${list }" var="item" >
                                           	   <tr>
	                                                <td>${item.payment_seq }</td>
	                                                <td>${item.payment_num }</td>
	                                                <td>${item.member_seq }</td>
	                                                <td>${item.price }</td>
	                                                <td>${item.payment_type }</td>
	                                                <td>${item.status_code }</td>
	                                                <td>${item.product_code }</td>
	                                                <td class="go-detail-item" data-seq="${item.content_seq}">${item.content_seq }</td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    
                                    </div>
                                 
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       </div>
        <!-- Static Table End -->
