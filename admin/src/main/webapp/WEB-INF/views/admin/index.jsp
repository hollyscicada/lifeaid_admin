<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%>

<script>

	$(document).ready(function(){

	setTimeout(function(){
		$("#payment").fadeIn(1000);
	},500)
	setTimeout(function(){
		$("#cs").fadeIn(1000);
	},500)
	})




</script>




<!-- Start Welcome area -->
<div class="all-content-wrapper" style="min-height: 900px; background: white; padding-left:20px;  padding-right:20px;">
			<div class="row"  style=" padding-top:50px;">
						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:200px; border: solid 1px gray; ">
							<div>
								<div>
									<div class="col-sm-6" >
										<div class="card-box">
											<div class="card-box content">											
												<div id="data" class="box box-header">
													<h5 class="card-title mb-4" style="text-align:center; border: solid 1px gray;">회원가입</h5>
												</div>
												<div class="box box-content" >
													<table>
														<tr>
															<td>Today</td>
															<td>전체</td>
														</tr>
														<tr>	
															<td>test</td>
															<td>test</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
										<div class="card">
											<div style="border: solid 1px gray;">													
												<span class="text-muted">
												<select>
													<option value="week">1주일</option>
													<option value="month">1개월</option>
													<option value="all">전체</option>
												</select></span>
												<h5 class="card-title mb-4" style="text-align:center;border: solid 1px gray;">방문자 수</h5>
												<h1 class="mt-1 mb-3">14.212</h1>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="card">
											<div style="border: solid 1px gray;">
												<h5 class="card-title mb-4" style="text-align:center; border: solid 1px gray;">프리미엄 회원</h5>
												<span class="text-muted">
													<select>
														<option value="week">1주일</option>
														<option value="month">1개월</option>
														<option value="all">전체</option>
											</select></span>
												<h1 class="mt-1 mb-3">${pre_member_count}</h1>
												<div class="mb-1">
													<span class="text-success"> <i class="mdi mdi-arrow-bottom-right"></i> 6.65% </span>

												</div>
											</div>
										</div>
										<div style="border: solid 1px gray;">
											<div >
												<h5 class="card-title mb-4" style="text-align:center; border: solid 1px gray;">구매/결제</h5>
												<h1 class="mt-1 mb-3"> ${payment_count} / </h1>
												<div class="mb-1">
													<span class="text-danger"> <i class="mdi mdi-arrow-bottom-right"></i> -2.25% </span>
													<span class="text-muted"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
	
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="border: solid 1px gray;">
							<div class="w-100">
								<div class="row">
									<div class="col-sm-6">
										<div class="card">
											<div class="card-header">
												<h5 class="card-title mb-4">신규 회원가입</h5>
												<select>
													<option value="week">1주일</option>
													<option value="month">1개월</option>
													<option value="all">전체</option>
												</select></span>
											</div>
											<div class="card-body">
												<h1 class="mt-1 mb-3">2.382</h1>
											</div>
										</div>
										<div class="card">
											<div class="card-body">
												<h5 class="card-title mb-4">구매건수</h5>
												<select>
													<option value="week">1주일</option>
													<option value="month">1개월</option>
													<option value="all">전체</option>
												</select></span>
												<h1 class="mt-1 mb-3">${payment_count}</h1>
											</div>
										</div>
										<div class="card">
											<div class="card-body">
												<h5 class="card-title mb-4">구매/결제액</h5>
												<select>
													<option value="week">1주일</option>
													<option value="month">1개월</option>
													<option value="all">전체</option>
												</select></span>
												<h1 class="mt-1 mb-3"> ${payment_count}</h1>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-body">
												<h5 class="card-title mb-4">등록 영상수</h5>
												<h1 class="mt-1 mb-3">${content_count}</h1>
											</div>
										</div>
										<div class="card">
											<div class="card-body">
												<h5 class="card-title mb-4">등록 E-Book컨텐츠</h5>
												<h1 class="mt-1 mb-3">${e_content_count}</h1>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
	</div>
					
					<div class="row">
						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<div class="card flex-fill">
									<div class="card-header">
										<h5 class="card-title mb-0">최근 결제내역</h5><span></span>
										<a href="/supervise/payment/list">+ 더보기</a>
									</div>
			
									   <table id="payment" style="display: none" data-toggle="table" data-pagination="true" data-show-pagination-switch="true">
                                        			<thead>
						                                	<tr>
																<th>결제일시</th>
																<th>컨텐츠구매</th>
																<th>결제금액</th>
																<th>결제방법</th>
																<th>회원명</th>
															</tr>
													</thead>
													  <tbody>		
						                                   <c:forEach var="payment" items="${payment }">
						                                   	<tr>
						                                        <td>${payment.payment_seq}</td>
						                                        <td>${payment.payment_seq}</td>
						                                        <td>${payment.payment_seq}</td>
						                                        <td>${payment.payment_seq}</td>
						                                        <td>${payment.payment_seq}</td>
						                                    </tr>
						                                  </c:forEach>
						                      		</tbody>
						             </table>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<div class="card">
									<div class="card-header">
										<h5 class="card-title mb-0">C/S 1:1문의</h5>
											<span></span><a href="/supervise/cs/list">+ 더보기</a>
									</div>
									<table id="cs" style="display: none" data-toggle="table" data-pagination="true" data-show-pagination-switch="true">
						             	   		<thead>
						                                	<tr>
																<th>문의유형</th>
																<th>문의제목</th>
																<th>회원명</th>
																<th>작성일자</th>
																<th>처리상태</th>
																<th>답변수정</th>
															</tr>
												</thead>
												<tbody>		
													<c:forEach var="cs" items="${cs }">		
						                                    <tr>
						                                        <td>${cs.title }</td>
						                                        <td>${cs.title }</td>
						                                        <td>${cs.title }</td>
						                                        <td>${cs.title }</td>
						                                        <td>${cs.title }</td>
						                                        <td>${cs.title }</td>
															</tr>
													</c:forEach>
						                       </tbody>
						             </table>
								</div>
							</div>
	
	</div>
</div>


<!-- Static Table Start -->
