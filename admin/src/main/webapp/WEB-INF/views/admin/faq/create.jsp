<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	function goSubmit() {
		submitContents("submit-btn", "content");
	}
	$(function(){
		$(document).on("click", ".submit-btn", function(){
			var title = $("input[name=title]");
			var content = $("textarea[name=content]");
			var file_url = $("input[name=file_url]");
			var write_by = $("input[name=write_by]");
			var param = {
					"title" : title.val(),
					"content" : content.val(),
					"file_url" : file_url.val(),
					"write_by": write_by.val()
			}
			
			ajaxCallPost("/supervise/api/v1/faq/create", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/faq/list"
				}
			},function(){})
		})
	
		/*	 파일업로드
		
		$(".file_fun").change(function(){
			ajaxUpload($(".file_fun")[0].files[0], "faq", function(data_url) {
				$("input[name='file_url']").val(data_url);
			})
		})
		*/

		CKEDITOR.replace('faq_content'
                , {height: 500                                                  
                 });
		
		
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
						 <div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">FAQ 추가 / 수정</h1>
                                </div>
                            </div>
						<form>
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div class="row">
								<div style="margin-left:30px; margin-right:30px;">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                       
			                                <tbody>
			                                	<tr>
													<th>분류</th>
													<td>	
														<select name="search_type" class="form-control">
																<option value="title">제목</option>
																<option value="content">내용</option>
																<option value="faq_code">분류명</option>
														</select>
													</td>
												</tr>
			                                    <tr>
			                                        <th>제목</th>
													<td><input type="text" name="answer" class="form-control" ></td>
												</tr>
			                                   <tr>	
													<th>내용</th>
													<td><textarea id="faq_content" name="content" class="form-control"></textarea>
													</td>
						                       </tr>

			                                </tbody>
			                            </table>
								</div>
							</div>
						</form>
					
								<div class="payment-adress">
									<button class="btn btn-primary waves-effect waves-light submit-btn" type="button" >저장</button>
									<button type="button" class="btn btn-primary waves-effect waves-light list-btn">목록</button>
								</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Static Table End -->