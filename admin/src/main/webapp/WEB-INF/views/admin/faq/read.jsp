<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
			$(".btn-success").click(function(){
				if(confirm("정말 수정하시겠습니까?")){
					var seq = "${detail.faq_seq }";
					location.href="/supervise/faq/update/"+seq;
				}
			})
			$(".btn-danger").click(function(){
				if(confirm("정말 삭제하시겠습니까?")){
					var seq = "${detail.faq_seq }";
					ajaxCallGet("/supervise/api/v1/faq/remove?seq="+seq, function(res){
						if(res.success){
							alert("작업이 완료되었습니다.");
							location.href="/supervise/faq/list"
						}
					})
				}
			})
			
			var detail = "${detail.content }".replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&#039;/g,"'").replace(/&quot;/g,"\"");
	    	$("#content").html(detail);
			
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
						 <div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">FAQ 상세 보기</h1>
                                </div>
                         </div>
                        <form>
						<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div style="padding: 30px 30px 0 30px">
							<table class="table table-bordered table-striped x-editor-custom">
			                      <tbody>
			                                	<tr>
													<th>분류명</th>
			                                        <td>${detail.faq_seq}</td>
												</tr>
												<tr>
													<th>제목</th>
			                                        <td>${detail.title}</td>
												<tr>
													<th>등록일</th>
			                                        <td>${detail.content}</td>
												</tr>
												<tr>
													<th>내용</th>
			                                        <td>${detail.content}</td>
												</tr>
			                                    
	                                </tbody>
		                    </table>
						
						</div>	
						</form>
							<div class="payment-adress">
								<button class="btn btn-primary waves-effect waves-light btn-success" type="button" >수정</button>
								<button type="button" class="btn btn-primary waves-effect waves-light btn-danger">삭제</button>
							</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
