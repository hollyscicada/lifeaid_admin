<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
<c:set var="name" value="faq" />
 <c:set var="seq" value="faq_seq" /> 
  
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(){
				location.href="/supervise/faq/update/"+ $(this).attr("data-seq");
    		})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
			setTimeout(function(){
				$("#select").fadeIn(1000);
			},500)
	    
			$( ".start" ).datepicker({
		        showOn: "both", 
		        buttonImage: "/resources/images/sub/icon_calender.jpg", 
		        buttonImageOnly: true ,
				dateFormat	: "yy-mm-dd",
		        
		    });
		    $( ".finish" ).datepicker({
		        showOn: "both", 
		        buttonImage: "/resources/images/sub/icon_calender.jpg", 
		        buttonImageOnly: true ,
		        dateFormat	: "yy-mm-dd"
		    });
			$(".btn-add").click(function(){
				location.href="/supervise/faq/create"
			});
			$( ".chkAll" ).click(function(){
				var chk= $(this).is(":checked");

				if(chk)$("")

				
				$("[name='chk']").prop("checked", $(this).is(":checked"));
	
			});	

	
    	})
    	
    	$(function(){
		$(".btn-remove").click(function(){

			
			// var category_1 = $("input[name=category_1]");
			// var category_2 = $("input[name=category_2]");

			var faq_seq = $("input[name=faq_seq]");

			
			var param = {
					"faq_seq" : faq_seq.val()
			}
			
			ajaxCallGet("/supervise/admin/v1/faq/delete", param, function(res){
				if(res.success){
					alert("삭제 되었습니다.");
					location.href="/supervise/faq/list"
				}
			},function(){})
		})
	})
	


		
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>FAQ 관리</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                    <div class="row">
                                    	<div>
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
            									<th>기본검색</th>
            								</tr>
            								<tr>
            									<td>
            										<select id="keyword_1" class="form-control">
		            									<option value="id">FAQ카테고리</option>
		            									<option value="name">분류/태그명</option>
		            								</select>
		            							</td>
		            							<td>
		      										<select id="keyword_1" class="form-control">
		            									<option value="id">제목</option>
		            									<option value="name">분류/태그명</option>
		            								</select>
		            							</td>
		            							<td>				
		            								<input type="text" id="keyword_2" class="form-control" >

												</td>
            								</tr>		
            									
            							</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px">조회</button>
										<button type="reset" class="btn btn-primary btn-success" style="float:right;">초기화</button>
			
                               </form>    
                               </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px">
                                    	<input type="hidden" value="${member.faq_seq}">
                                    	<span></span><h4>조회 건수 : ${fn:length(list)} 건</h4>
							        </div>
                                    <div class="row">
                                   
                                     <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                               <thead>
                                            <tr>
                                            	<th><input type="checkbox" id="chkAll" name="chkAll" class="chkAll"/></th>
                                            	<th>번호</th>
                                            	<th>분류명</th>
                                            	<th>제목</th>
                                            	<th>등록일</th>
                                            	<th>관리</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${list }" var="item" >
                                           	   	<tr>
	                                                <td><input type="checkbox" id="chk" name="chk" class="chk" value="${item.faq_seq }"/></td>
	                                                <td>${item.rownum }</td>
	                                                <td>${item.faq_code }</td>
	                                                <td>${item.content }</td>
	                                                <td>${item.create_dt_yyyymmdd }</td>
	                                                <td class="go-detail-item" data-seq="${item.faq_seq}">[수정]</td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    
                                    </div>
                                    
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
		            					<div style="float:left">
		            						<button type="button" class="btn btn-primary btn-remove ">선택삭제</button>
										</div>  
		            					<div style="float:right">
		            						<button type="button" class="btn btn-primary btn-add ">추가하기</button>
										</div>                            	
                            		</div>
                                 
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       </div>
        <!-- Static Table End -->
