<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
		$(".submit-btn").click(function(){
			var thumb_url = $("input[name=thumb_url]");
			var img_url = $("input[name=img_url]");
			var file_url = $("input[name=file_url]");
			var title = $("input[name=title]");
			var create_by = $("input[name=write_by]");
			var show_yn = $("input[name=show_yn]:checked");
			var subscribe_price = $("input[name=subscribe_price]");
			var nomal_price = $("input[name=nomal_price]");
			var price = $("input[name=price]");
			var content = $("textarea[name=content]");
			var cancel_content = $("textarea[name=cancel_content]");
			var classify_type = "E";

			
			var param = {
					"img_url" : img_url.val(),
					"thumb_url" : thumb_url.val(),
					"file_url" : file_url.val(),
					"title" : title.val(),
					"create_by" : create_by.val(),
					"show_yn" : show_yn.val(),
					"subscribe_price": subscribe_price.val(),
					"nomal_price" : nomal_price.val(),
					"price" : price.val(),
					"content" : content.val(),
					"cancel_content" : cancel_content.val(),
					"classify_type" : classify_type
			}
			
			ajaxCallPost("/supervise/api/v1/contents/create", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/ebook/list";
				}
			},function(){})
		})
		$(".file_fun").change(function(){
			ajaxUpload($(".file_fun")[0].files[0], "contents", function(data_url) {
				$("input[name='file_url']").val(data_url);
			})
		})
		$(".img_fun").change(function(){
			ajaxUpload($(".img_fun")[0].files[0], "contents", function(data_url) {
				$("input[name='img_url']").val(data_url);
			})
		})
		$(".thumb_fun").change(function(){
			ajaxUpload($(".thumb_fun")[0].files[0], "contents", function(data_url) {
				$("input[name='thumb_url']").val(data_url);
			})
		})

		
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



		<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">E-Book 컨텐츠 추가 / 수정</h1>
                                </div>
                            </div>
						<form>
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div class="row">
								<div style="margin-left:30px; margin-right:30px;">
										<table id="user" class="table table-bordered x-editor-custom">
			                       
			                                <tbody>
			                                	<tr>
													<th>컨텐츠 제목 *</th>
													<td><input name="title" type="text" class="form-control" placeholder="제목을 입력해주세요"  value="${detail.title}"></td>
												</tr>
			                                    <tr>
			                                        <th>썸네일 *</th>
													<td>
														${detail.thumb_url}
														<input name="thumb_url" type="hidden" class="form-control">
														<input name="" type="file" class="form-control thumb_fun"  value="${detail.thumb_url}"></td>
												</tr>
			                                   <tr>	
													<th>이미지 *</th>
													<td>
														${detail.img_url}
														<input name="img_url" type="hidden" class="form-control" >
														<input name="" type="file" class="form-control img_fun"  value="${detail.img_url}">
												  	</td>
						                       </tr>
				                               <tr>   
			                                        <th>컨텐츠파일 *</th>
													<td>
														${detail.file_url}
														<input name="file_url" type="hidden" class="form-control" >
														<input name="" type="file" class="form-control file_fun" value="${detail.file_url}">
												  </td>
												</tr>	
			                                    <tr>
			                                        <th>컨텐츠 설명 *</th>
													<td>
														<textarea name="content" rows="10" cols="100" class="form-control" >${detail.content}</textarea>
													</td>
												</tr>
			                                    <tr>
													<th>취소/환불 정책 *</th>
													<td>
														<textarea name="cancel_content" rows="10" cols="100" class="form-control">${detail.cancel_content}</textarea>
													</td>
			                                    </tr>
												<tr>	
			                                      <th>연관 컨텐츠</th>
												  <td>		
													<input name="r_content_seq" type="text" class="form-control">
													<span><input name="" type="button" value="컨텐츠 선택" class="insert_btn" ></span>
												  </td>
			                                    </tr>
			                                    <tr>
			                                        <th>상태 *</th>
													<td>
														<input name="show_yn" type="radio" value="Y" checked>
														<span>게시</span>
														<input name="show_yn" type="radio" value="N">
														<span>게시 안함</span>
													</td>
												</tr>
			                                    <tr>
			                                        <th>가격 *</th>
													<td class="form-control"> 
														정상가 :  <input name="nomal_price" type="text" value="${detail.nomal_price}">
														판매가 :  <input name="price" type="text" value="${detail.price}">
														구독 회원가 :  <input name="subscribe_price" type="text" value="${detail.subscribe_price}">
													</td>
												</tr>
			                                </tbody>
			                            </table>
								</div>
							</div>
						</form>
							<div style="float:right;">
								<button type="button" class="btn btn-primary waves-effect waves-light submit-btn">저장</button>
								<button type="button" class="btn btn-primary waves-effect waves-light cancel-btn">취소</button>
							</div>			
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
