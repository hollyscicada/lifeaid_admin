<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
    
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(){
    			location.href="/supervise/category/"+$(this).attr("data-seq")
    		})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
			
	
    		$(document).on("click", ".go-update-item", function(){
    			if(confirm("수정하시겠습니까?")){
    			location.href="/supervise/category/update"+$(this).attr("data-seq")
    			}
    		})

			$(".go-delete-item").click(function(){
				if(confirm("정말 삭제하시겠습니까?")){
					ajaxCallGet("/supervise/admin/v1/category/remove?seq="+$(this).attr("data-seq"),

					 function(res){
						if(res.success){
							alert("삭제되었습니다.");
							location.href="/supervise/category/list"
						}
					})
				}
			})
			
			$(".btn-create").click(function(){
				if(confirm("카테고리 추가하시겠습니까?")){
					var sort = $("input[name=sort]");
					var name = $("input[name=name]");
					var write_by = $("input[name=write_by]");
					
					var param = {
							"sort" : sort.val(),
							"name" : name.val(),
							"write_by": write_by.val()
					}

					
					ajaxCallPost("/supervise/admin/v1/category/create", param, function(res){
						if(res.success){
	
							alert("추가되었습니다.");
							location.href="/supervise/category/list"
						}
					},function(){})
				}
			})

    	})
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>카테고리 관리</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                   <input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
									  <div class="row">
                                    	<div>
                                    	<table class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
												<th>카테고리 분류</th>
            									<td colspan="3">
            										<select id="category">
														<option value="6">1차 분류 선택</option>
														<c:forEach var="category" items="${category }" varStatus="state">
															<option value="${category.category_seq }">${category.name }</option>
														</c:forEach>
            										</select>
            										<select>
														<option value="">2차 분류 선택</option>
														<c:forEach var="category" items="${category }" varStatus="state">
															<option value="${category.category_seq }">${category.name }</option>
														</c:forEach>
            										</select>
            										<select>
														<option value="">3차 분류 선택</option>
														<c:forEach var="category" items="${category }" varStatus="state">
															<option value="${category.category_seq }">${category.name }</option>
														</c:forEach>
            										</select>
            										<select>
														<option value="">4차 분류 선택</option>
														<c:forEach var="category" items="${category }" varStatus="state">
															<option value="${category.category_seq }">${category.name }</option>
														</c:forEach>
            										</select>
												</td>
            								</tr>
            								<tr>		
            									<th>카테고리명</th>
            									<td colspan="3">
            									<input type="text" class="form-control" name="name"/>
            									</td>
            								</tr>
            								<tr>		
            									<th>카테고리 설명</th>
            									<td colspan="3">
            										<textarea rows="50" cols="100" class="form-control" name="content"></textarea>
            									</td>
            								</tr>
            								<tr>	
            									<th>분류 구분</th>
            									<td colspan="3">
            										<input type="radio" class="form-control-sm" name="category_type" value="C">
            										<span>영상 컨텐츠</span>
            										<input type="radio" class="form-control-sm" name="category_type" value="E">
            										<span>E-Book 컨텐츠</span>
            										<input type="radio" class="form-control-sm" name="category_type" value="F">
													<span>FAQ</span>	
												</td>
            								</tr>
            								
            							</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four btn-create" style="float:right; margin-left:10px">저장</button>
			
                               </form>    
                               
                               </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px">
                                    	<input type="hidden" value="${member.member_seq}">
										<h4 >조회 건수 : <span class="listlength">${fn:length(list)}</span> 건</h4>
							        </div>
                                    <div class="row">
                                   
                                     <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                               <thead>
                                            <tr>
                                            	<th>카테고리번호</th>
                                            	<th>카테고리명</th>
                                            	<th>카테고리분류</th>
                                            	<th>관리</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody">
                                            <c:forEach items="${list }" var="item" >
                                           	   <tr>
	                                                <td class="go-detail-item" data-seq="${item.category_seq}">${item.category_seq }</td>
	                                                <td>${item.name }</td>
	                                                <td>${item.sort }</td>
	                                                <td><a href="#" class="go-update-item" data-seq="${item.category_seq}">[수정]</a> 
	                                                	<a href="#" class="go-delete-item" data-seq="${item.category_seq}">[삭제]</a>
	                                                </td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    
                                    </div>
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Static Table End -->
