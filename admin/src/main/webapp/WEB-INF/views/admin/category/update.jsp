<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>

function getCategory(seq){
			
			var category_seq = seq;
			
			var param = {
				//	"admin_seq" : "${detail.admin_seq}",
				//	"admin_id" : admin_id.val(),
					"category_seq" : category_seq
			}
			
		ajaxCallPost("/supervise/admin/v1/category/category_list" , param, function(res){
			var ss = "";
				for(var i = 0; i < res.data.length ; i++){
					  ss +=  `<option value="`+res.data[i].info_seq+`">`+res.data[i].name+`</option>`;
	                   $(".category_info").html(ss)
				}
	
		},function(){})

}
	
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
	 						<div style="margin-top:50px; margin-bottom:30px;">
                                <div>
                                    <h1>카테고리 관리</h1>
                                </div>
                            </div>     
						<form id="categoryform">
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
								<div style="padding: 20px 0 20px 10px;">
								
								<div class="col-lg-2" style="border: solid 1px gray; padding: 10px 10px 10px 10px; margin-right:30px;">
									<h3>1차분류</h3>
										
										<p class="cg">
											<select size="20" name="category_seq" id="category_seq" class="form-control">
												<c:forEach var="category" items="${category }" varStatus="state">
													<option value="${category.category_seq }" onclick="getCategory('${category.category_seq }')">${category.name }</option>
												</c:forEach>
											</select>
										</p>
										
										<p class="cg_bt">
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_up.jpg"></a>
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_down.jpg"></a>
											<a href="#" onclick=""><button type="button" class="">저장</button></a>
										</p>
										
								</div>
								<div class="col-lg-2" style="border: solid 1px gray; padding: 10px 10px 10px 10px; margin-right:30px;">
									<h3>2차분류</h3>
									
										<p class="cg-1">
											<select size="20" name="category_1" class="form-control category_info">
													<option value="${category_info.gubun_seq }">${category_info.name }</option>
											</select>	
										</p>
										
										<p class="cg_bt">
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_up.jpg"></a>
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_down.jpg"></a>
											<a href="#" onclick=""><button type="button" class="">저장</button></a>
										</p>
								</div>
								<div class="col-lg-2" style="border: solid 1px rgb(128, 128, 128); padding: 10px 10px 10px 10px; margin-right:30px;">
									<h3>3차분류</h3> 
										<p class="cg">
											<select size="20" name="category_1" id="" class="form-control">
												<c:forEach var="category_gubun" items="${category_gubun }" varStatus="state">
													<option value="${category_gubun.gubun_seq }">${category_gubun.name }</option>
												</c:forEach>
											</select>	
										</p>
										
										<p class="cg_bt">
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_up.jpg"></a>
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_down.jpg"></a>
											<a href="#" onclick=""><button type="button" class="">저장</button></a>
										</p>
									
								</div>
								<div class="col-lg-2" style="border: solid 1px gray; padding: 10px 10px 10px 10px; margin-right:30px;">
									<h3>4차분류</h3>
										<p class="cg">
											<select size="20" name="category_1" id="" class="form-control">
												<c:forEach var="category" items="${category }" varStatus="state">
													<option value="${category.category_seq }">${category.name }</option>
												</c:forEach>
											</select>
										</p>
										
										<p class="cg_bt">
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_up.jpg"></a>
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_down.jpg"></a>
											<a href="#" onclick=""><button type="button" class="">저장</button></a>
										</p>
								
								</div>
								<div class="col-lg-2" style="border: solid 1px gray; padding: 10px 10px 10px 10px; margin-right:30px;">
									<h3>5차분류</h3>
										<p class="cg">
											<select size="20" name="category_1" id="" class="form-control">
												<c:forEach var="category" items="${category }" varStatus="state">
													<option value="${category.category_seq }">${category.name }</option>
												</c:forEach>
											</select>
										</p>
										
										<p class="cg_bt" >
											<a href="#" onclick=""><img alt="위로" src="/resources/images/sub/btn_up.jpg"></a>
											<a href="#" onclick=""><img alt="" src="/resources/images/sub/btn_down.jpg"></a>
											<a href="#" onclick=""><button type="button" class="">저장</button></a>
										</p>
								</div>
	
							</div>
						</form>
						</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:50px;">
								<div style="background:#C8FAC8; padding: 20px 0 20px 10px;">
									<p>[참조하세요]</p>
										<ul><a>1. 카테고리 순위변경 안내</a>
											<li>1차 카테고리명을 먼저 클릭하시면 2차~5차까지 순차적으로 항목이 노출 됩니다.</li>
											<li></li>
										</ul><br>
											
										<ul><a>2. 버튼 사용방법 안내</a>
											<li>[위 로] 버튼을 클릭시 상위로 한칸씩 이동되며 우선순위로 노출 됩니다.</li>
											<li>[아래로] 버튼을 클릭시 하위로 한칸씩 이동됩니다.</li>
											<li>[저 장] 버튼을 클릭시 해당 카테고리 항목만 처리되며 그외 카테고리는 처리되지 않습니다.</li>
										</ul>
								</div>
								
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Static Table End -->
