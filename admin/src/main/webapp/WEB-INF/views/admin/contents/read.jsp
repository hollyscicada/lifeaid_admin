<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
  
<script>
	$(function(){
	
		$("#select-category-btn").click(function(){

	         $('.popup_category').fadeIn();
       
		})

		$("#select-content-btn").click(function(){

	         $('.popup_content').fadeIn();
       
		})

		$("#select-tag-btn").click(function(){

	         $('.popup_tag').fadeIn();
       
		})				

		CKEDITOR.replace('content'
                , {height: 500                                                  
        });

		$(".file_fun").change(function(){
			ajaxUpload($(".file_fun")[0].files[0], "contents", function(data_url) {
				$("input[name='file_url']").val(data_url);
			})
		})
		$(".img_fun").change(function(){
			ajaxUpload($(".img_fun")[0].files[0], "contents", function(data_url) {
				$("input[name='img_url']").val(data_url);
			})
		})
		$(".thumb_fun").change(function(){
			ajaxUpload($(".thumb_fun")[0].files[0], "contents", function(data_url) {
				$("input[name='thumb_url']").val(data_url);
			})
		})

		$(".submit-btn").click(function(){
			var thumb_url = $("input[name=thumb_url]");
			var title = $("input[name=title]");
			var create_by = $("input[name=write_by]");
			var video_type_code = $("input[name=premium_seq]:checked");
			var video_control_code = $("input[name=category_seq]:checked");
			var access_code = $("input[name=access_code]:checked");
			var content = $("textarea[name=content]");
			var classify_type = "C";

			
			var param = {
					"title" : title.val(),
					"thumb_url" : thumb_url.val(),
					"create_by": create_by.val(),
					"video_type_code" : video_type_code.val(),
					"video_control_code" : video_control_code.val(),
					"access_code" : access_code.val(),
					"content" : content.val(),
					"classify_type" : classify_type
			}
			
			ajaxCallPost("/supervise/api/v1/contents/create", param, function(res){
				if(res.success){
					alert("컨텐츠 등록이 완료되었습니다.");
					location.href="/supervise/contents/list"
				}
			},function(){})
		})
	})

	
	
</script>



<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



		<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">영상 컨텐츠 추가 / 수정</h1>
                                </div>
                            </div>
						<form>
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div class="row">
								<div style="margin-left:30px; margin-right:30px;">
										<table id="user" class="table table-bordered x-editor-custom">
			                       
			                                <tbody>
			                                	<tr>
													<th>컨텐츠 제목 *</th>
													<td><input name="title" type="text" class="form-control" placeholder="제목을 입력해주세요"></td>
												</tr>
			                                    <tr>
			                                        <th>썸네일 *</th>
													<td>
														<input name="thumb_url" type="hidden" class="form-control" >
														<input name="" type="file" class="form-control thumb_fun" >
													</td>
												</tr>
			                                   <tr>	
													<th>카테고리 선택 *</th>
													<td>
														  <input name="category_txt" type="text" class="form-control">
													<span><input name="" type="button" value="카테고리 선택" id="select-category-btn" class="btn"></span>
												  	</td>
						                       </tr> 
				                               <tr>   
			                                        <th>분류 태그 *</th>
													<td>
														  <input name="classify_type" type="text" class="form-control">
													<span><input name="" type="button" value="분류태그 선택" id="select-tag-btn" class="btn"></span>
												  </td>
												</tr>
			                                    <tr>
													<th>영상타입 / 영상 아이디 *</th>
													<td>
													<select name="video_type_code" class="form-control">
														<option value="0">연결없음</option>
														<option value="1">URL링크</option>
														<option value="2">YouTube</option>
														<option value="3">비메오</option>
													</select></td>
			                                    </tr>

			                                    <tr>
													<th>컨텐츠 Control *</th>
													<td class="form-control">
														영상 플레이어 :
														<input name="video_control_code" type="radio" value="0" checked>
														<span>기본 player</span>
														<input name="video_control_code" type="radio" value="1">
														<span>라이프에이드 player</span>
													</td>
												</tr>
			                                    <tr>
													<th>접근제한 / 중요도 *</th>
													<td class="form-control">
														접근제한 : 
														<input name="access_code" type="radio" value="0" checked>
														<span>누구나</span>
														<input name="access_code" type="radio" value="1">
														<span>멤버만</span>
														<input name="access_code" type="radio" value="2">
														<span>구독자만</span>
													</td>
												</tr>
												<tr>	
			                                      <th>연관 컨텐츠</th>
												  <td>		
													<input name="r_content_seq" type="text" class="form-control">
													<span><input name="email_check" type="button" value="컨텐츠 선택" id="select-content-btn" class="btn btn-primary waves-effect waves-light check-btn"></span>
												  </td>
			                                    </tr>
			                                    <tr>
			                                        <th>상태</th>
													<td>
														<input name="show_yn" type="radio" value="Y" checked>
														<span>게시</span>
														<input name="show_yn" type="radio" value="N">
														<span>게시 안함</span>
													</td>
												</tr>
			                                    <tr>
			                                        <th>컨텐츠 소개 *</th>
													<td>
														<textarea id="content" name="content" class="form-control">${detail.content}</textarea>
													</td>
												</tr>
			                                </tbody>
			                            </table>
								</div>
							</div>
						</form>
							<div style="float:right;">
								<button type="button" class="btn btn-primary waves-effect waves-light submit-btn">저장</button>
								<button type="button" class="btn btn-primary waves-effect waves-light cancel-btn">취소</button>
							</div>			
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->

<!-- popup -->
       <div class="popup-bg"></div>
        <div class="popup position-center popup_category">
          <p class="font-20r font-xs-16r">카테고리 선택</p>
          <hr/>
          	<table>
         		<tr>
         			<th>카테고리1</th>
         			<th>카테고리2</th>
         			<th>카테고리3</th>
         			<th>카테고리4</th>
         		</tr> 	
          		<tr>
          			<td>
          				<select size="4">
          					<option>1</option>
          					<option>2</option>
          					<option>3</option>
          					<option>4</option>
          				</select>
          			</td>
           			<td>
          				<select size="4">
          					<option>1</option>
          					<option>2</option>
          					<option>3</option>
          					<option>4</option>
          				</select>
          			</td>          			
          			<td>
          				<select size="4">
          					<option>1</option>
          					<option>2</option>
          					<option>3</option>
          					<option>4</option>
          				</select>
          			</td>          			
          			<td>
          				<select size="4">
          					<option>1</option>
          					<option>2</option>
          					<option>3</option>
          					<option>4</option>
          				</select>
          			</td>
          		</tr>
          	
          	</table>
          <input type="button" value="확인" class="popup-btn" onclick="">
        </div>

        <div class="popup position-center popup_content">
          <p class="font-20r font-xs-16r">컨텐츠 선택 - 영상</p>
          <hr/>
          	<table>
         		<tr>
         			<th>컨텐츠제목</th>
         			<th>썸네일</th>
         			<th>카테고리</th>
         		</tr> 	
          		<tr>
          			<td>
          				content
          			</td>
           			<td>
						<input name="img_url" type="hidden">
          				<input type="file" value="img" name="" class="img_fun">
          			</td>          			
          			<td>
          				<a href="#">카테고리</a>
          			</td>          			
          		</tr>
          	
          	</table>
          <input type="button" value="확인" class="popup-btn" onclick="">
        </div>

        <div class="popup position-center popup_tag">
        	<div>
       		   <p class="font-20r font-xs-16r">분류 태그 선택</p>
        		<a href="#">닫기</a>
        	</div>
          <hr/>
          	<table>
         		<tr>
         			<th>분류선택</th>
					<td>
						<input type="radio">운동
						<input type="radio">체형교정
						<input type="radio">마사지/스트레칭
						<input type="radio">재활운동
					</td>
         		</tr> 	
          		<tr>
          			<th>태그 1 선택</th>
          			<td>tag1</td>
          		</tr> 	
          		<tr>
          			<th>태그 2 선택</th>
          			<td>tag2</td>
          		</tr>
          	</table>
          <input type="button" value="확인" class="popup-btn">
        </div>

