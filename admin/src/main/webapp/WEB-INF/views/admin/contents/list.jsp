<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
<c:set var="name" value="content" />
<c:set var="seq" value="content_seq" />       
       
     
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(){
    			location.href="/supervise/contents/update/"+$(this).attr("data-seq")
    		})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
			setTimeout(function(){
				$("#select").fadeIn(1000);
			},500)
			
			$(".btn-add").click(function(){
					location.href="/supervise/contents/create";
			})		    


    	})
    	
    	$(function(){
		$(".btn-success").click(function(){
			var start_1 = $("input[name=start_1]");
			var finish_1 = $("input[name=finish_1]");
			var start_2 = $("input[name=start_2]");
			var finish_2 = $("input[name=finish_2]");

			
			var category_1 = $("input[name=category_1]");
			var category_2 = $("input[name=category_2]");
			var category_3 = $("input[name=category_3]");
			var category_4 = $("input[name=category_4]");
			var keyword_1 = $("input[name=keyword_1]");
			var keyword_2 = $("input[name=keyword_2]");
			var member_state = $("input[name=member_state]");
			var phone = $("input[name=phone]");
			var premium_seq = $("input[name=premium_seq]");
			var category_seq = $("input[name=category_seq]");

			
			var param = {
					"member_id" : member_id.val(),
					"password" : password.val(),
					"name" : name.val(),
					"write_by": write_by.val(),
					"sex" : sex.val(),
					"yyyy" : yyyy.val(),
					"email" : email.val(),
					"phone" : phone.val(),
					"premium_seq" : premium_seq.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/contents/create", param, function(res){
				if(res.success){
					alert("회원 등록이 완료되었습니다.");
					location.href="/supervise/contents/list"
				}
			},function(){})
			})
		})
	


		
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>영상 컨텐츠 관리</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                    <div class="row">
                                    	<div>
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
            									<th>컨텐츠 구분</th>
            									<td colspan="3">
            										<input type="radio" class="datepicker form-control-sm" name="premium_yn"/>
            										<span>일반</span>
            										<input type="radio" class="datepicker form-control-sm" name="premium_yn"/>
            										<span>프리미엄</span>
												</td>
            										
            									<th>카테고리</th>
            									<td colspan="3">
		            									<select id="category_1" class="form-control">
		            											<option value="all">카테고리1</option>
		            											<c:forEach var="category" items="${category }">
		            											<option value="${category.category_seq }">${category.name }</option>
		            											</c:forEach>
		            									</select>
		            									<select id="category_1" class="form-control">
		            											<option value="all">카테고리2</option>
		            											<c:forEach var="category" items="${category }">
		            											<option value="${category.category_seq }">${category.name }</option>
		            											</c:forEach>
		            									</select>
		            									<select id="category_1" class="form-control">
		            											<option value="all">카테고리3</option>
		            											<c:forEach var="category" items="${category }">
		            											<option value="${category.category_seq }">${category.name }</option>
		            											</c:forEach>
		            									</select>
            									</td>
            								</tr>
            								<tr>		
            									<th>컨텐츠 등록일자</th>
            									<td >
            										<input type="date" class="datepicker form-control-sm" name="start_1"/>
            										<a href="#"></a> ~ 
            										<input type="date" class="datepicker form-control-sm" name="finish_1"/>
												</td>
            									<th>관심카테고리 구분</th>
            									<td colspan="3">
		            									<select id="category" class="form-control">
		            											<option value="all">전체 / 운동 /체형교정 /통증</option>
		            											<c:forEach var="category" items="${category }" varStatus="state">
		            												<option value="${category.category_seq }">${category.name }</option>
		            											</c:forEach>
		            									</select>	
												</td>
            					
            								</tr>
            								<tr>
            									<th>검색어 입력</th>
            									<td class="form-control">		            									
            											<select id="keyword_1" >
		            										<option value="id">영상제목명</option>
		            										<option value="name">회원명</option>
		            										<option value="email">이메일</option>
		            										<option value="phone">휴대전화</option>
		            									</select>
		            					
		            								<input type="text" id="keyword_2">
		            							</td>
            								</tr>
            							</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px">조회</button>
										<button type="reset" class="btn btn-primary btn-success" style="float:right;">초기화</button>
			
                               </form>    
                               </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px">
                                    	<input type="hidden" value="">
                                    	<span></span><h4>조회 건수 : ${fn:length(list)} 건</h4>
							        </div>
                                    <div class="row">
                                   
                                     <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                               <thead>
                                            <tr>
                       	                    	<th>No</th>
                                            	<th>컨텐츠 코드</th>
                                            	<th>구분</th>
                                            	<th>컨텐츠 제목</th>
                                            	<th>카테고리</th>
                                            	<th>분류/태그</th>
                                            	<th>조회수</th>
                                              	<th>즐겨찾기수</th>
                                            	<th>게시구분</th>
                                            	<th>등록일</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           	   <c:forEach items="${list }" var="item" >
												<c:set var="temp_seq" value="${item.content_seq }" />
                                           	   <tr>
	                                                <td>${item.content_seq }</td>
	                                                <td class="go-detail-item" data-seq="${item.content_seq}">${item.content_seq }</td>
	                                                <td>${item.classify_type }</td>
	                                                <td>${item.title }</td>
	                                                <td>${item.gubun_seq }</td>
	                                                <td>${item.gubun_seq }</td>
	                                                <td>${item.gubun_seq }</td>
													<td>${item.gubun_seq }</td>
													<%@ include file="/WEB-INF/views/admin/common/showYn.jsp" %>
													<td>${item.create_dt_yyyymmdd }</td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                    	<div style="float:right">
		            					<button type="button" class="btn btn-primary btn-add ">+ 추가하기</button>
										</div>                            	
                            		</div>
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Static Table End -->
