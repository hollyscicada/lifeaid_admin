<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
			$(".btn-danger").click(function(){
				if(confirm("정말 삭제하시겠습니까?")){
					var seq = "${detail.contact_seq }";
					ajaxCallGet("/supervise/api/v1/contact/remove?seq="+seq, function(res){
						if(res.success){
							alert("작업이 완료되었습니다.");
							location.href="/supervise/contact/list"
						}
					})
				}
			})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>문의하기 <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th>사용자 정보</th>
													<th>결과</th>
												</tr>
			                                    <tr>
			                                        <td>전화번호</td>
			                                        <td>${detail.phone }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>이메일</td>
			                                         <td>${detail.email }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>생성일자</td>
			                                        <td>${detail.create_dt_yyyymmdd }</td>
			                                    </tr>
			                                </tbody>
			                            </table>
			                            
			                            
			                            <table id="" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th>문의 정보</th>
													<th>결과</th>
												</tr>
			                                    <tr>
			                                        <td>문의구분</td>
			                                        <td>${detail.name }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>첨부파일명</td>
			                                         <td>${detail.file_url }</td>
			                                    </tr>
			                                </tbody>
			                            </table>
									</div>
									
									
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<table id="" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th>문의내용</th>
												</tr>
			                                    <tr>
			                                        <td>${detail.content}</td>
			                                    </tr>
			                                </tbody>
			                            </table>
									
										<table id="" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th>첨부파일</th>
												</tr>
			                                    <tr>
			                                        <td><img src="${detail.file_url}"></td>
			                                    </tr>
			                                </tbody>
			                            </table>
									</div>
								</div>
							</form>
						</div>
						
							<button type="button" class="btn btn-custon-four btn-danger" style="float:right;margin-left:10px">삭제</button>
<!-- 						<button type="button" class="btn btn-custon-four btn-success"style="float:right;">수정</button> -->
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
