<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 	<script src="/resources/admin/js/data-table/bootstrap-table.js"></script>
    <script src="/resources/admin/js/data-table/tableExport.js"></script>
    <script src="/resources/admin/js/data-table/data-table-active.js"></script>
    <script src="/resources/admin/js/data-table/bootstrap-table-editable.js"></script>
    <script src="/resources/admin/js/data-table/bootstrap-table-resizable.js"></script>
    <script src="/resources/admin/js/data-table/colResizable-1.5.source.js"></script>
    <script src="/resources/admin/js/data-table/bootstrap-table-export.js"></script>
    
    <link rel="stylesheet" href="/resources/admin/css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="/resources/admin/css/data-table/bootstrap-editable.css">