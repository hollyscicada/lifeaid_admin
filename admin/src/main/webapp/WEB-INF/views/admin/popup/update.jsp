<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	function goSubmit() {
		submitContents("submit-btn", "content");
	}
	$(function(){
		$(document).on("click", ".submit-btn", function(){
			var notice_seq = "${detail.popup_seq}";
			var title = $("input[name=title]");
			var content = $("textarea[name=content]");
			var file_url = $("input[name=file_url]");
			var write_by = $("input[name=write_by]");
			
			
			var param = {
					"notice_seq":notice_seq,
					"title" : title.val(),
					"content" : content.val(),
					"file_url" : file_url.val(),
					"write_by": write_by.val()
			}
			
			ajaxCallPost("/supervise/api/v1/notice/update", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/notice/list"
				}
			},function(){})
		})
		
		/*
			 파일업로드
		*/
		$(".file_fun").change(function(){
			ajaxUpload($(".file_fun")[0].files[0], "notice", function(data_url) {
				$("input[name='file_url']").val(data_url);
				$("#file_txt").text(data_url);
			})
		})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



		<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
				 <div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">팝업 추가 / 수정</h1>
                                </div>
                            </div>
                        	<form>
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div class="row">
								<div style="margin-left:30px; margin-right:30px;">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                       
			                                <tbody>
			                                	<tr>
													<th>접속기기</th>
													<td>
														<select>
															<option>PC</option>
														</select>
													</td>
												</tr>
			                                    <tr>
			                                        <th>팝업크기(pixel)</th>
													<td><input type="text" name="password" class="form-control-sm" value="비밀번호"> X 
														<input type="text" name="password" class="form-control-sm" value="비밀번호">
													</td>
												</tr>
			                                   <tr>	
													<th>팝업위치(pixel)</th>
													<td><input type="text" name="password" class="form-control-sm" value="비밀번호"> X 
														<input type="text" name="password" class="form-control-sm" value="비밀번호">
													</td>
						                       </tr>
				                               <tr>   
			                                        <th>실행기간</th>
													<td><input name="name" type="text" class="form-control-sm" placeholder="이름"> - 
													<input name="name" type="text" class="form-control-sm" placeholder="이름">
													</td>
												</tr>
			                                    <tr>
													<th>노출여부</th>
													<td class="form-control">
														<input name="sex" type="radio" value="M" checked>
														<span>노출함</span>
														<input name="sex" type="radio" value="W">
														<span>노출안함</span>
													</td>
			                                    </tr>

			                                    <tr>
													<th>팝업제목</th>
													<td>
														<input name="title" type="text" class="form-control" placeholder="제목">
													</td>
												</tr>
			                                    <tr>
													<th>팝업내용</th>
													<td >
														<textarea id="ir1" rows="10" cols="300" name="content"class="form-control"></textarea>
														<span id="file_txt">${detail.popup_seq }</span>
														<input name="file_url" type="hidden" class="form-control" value="${detail.popup_seq }">
														<input name="" type="file" class="form-control file_fun" >
													
													</td>
												</tr>
												
			                                </tbody>
			                            </table>
								</div>
							</div>
						</form>
							<div style="float:right;">
								<button type="button" class="btn btn-primary waves-effect waves-light submit-btn">저장</button>
								<button type="button" class="btn btn-primary waves-effect waves-light cancel-btn">목록</button>
							</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Static Table End -->