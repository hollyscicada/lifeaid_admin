<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
			$(".btn-success").click(function(){
				if(confirm("정말 수정하시겠습니까?")){
					var seq = "${detail.popup_seq }";
					location.href="/supervise/popup/update/"+seq;
				}
			})
			$(".btn-danger").click(function(){
				if(confirm("정말 삭제하시겠습니까?")){
					var seq = "${detail.popup_seq }";
					ajaxCallGet("/supervise/admin/v1/popup/remove?seq="+seq, function(res){
						if(res.success){
							alert("작업이 완료되었습니다.");
							location.href="/supervise/popup/list"
						}
					})
				}
			})
			$(".btn-list").click(function(){
					location.href="/supervise/popup/list"
			})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 30px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div style="padding: 0 50px 0 0">
                                    <h1>팝업 관리</h1>
                                </div>
                            </div>
						<div style="padding: 30px 30px 0 30px">
							<table class="table table-bordered table-striped x-editor-custom">
			                      <tbody>
			                                	<tr>
													<th>접속기기</th>
												    <td>${detail.popup_seq }</td>
			                                    </tr>
			                                    <tr>
													<th>팝업크기</th>
												    <td>${detail.popup_seq}</td>
			                                	</tr>
			                                	<tr>    
			                                	    <th>팝업위치</th>
												    <td>${detail.popup_seq}</td>
			                                	</tr>
			                                	<tr>    
													<th>실행기간</th>
												    <td>${detail.popup_seq}</td>
			                                	</tr>
			                                	<tr>    
													<th>노출여부</th>
												    <td>${detail.popup_seq}</td>
			                                	</tr>
			                                	<tr>    
													<th>팝업제목</th>
												    <td>${detail.popup_seq}</td>
			                                	</tr>
			                                	<tr>    
													<th>팝업내용</th>
												    <td>${detail.popup_seq}</td>
			                                    </tr>
			                          </tbody>
		                    </table>
						
						</div>						

						<div style="padding: 0 30px 50px 30px">
						<button type="button" class="btn btn-custon-four btn-list" style="float:right;">목록</button>
						<button type="button" class="btn btn-custon-four btn-danger" style="float:right; margin-right:10px">삭제</button>
						<button type="button" class="btn btn-success" style="float:right; margin-right:10px">수정</button>
						</div>
					</div>
		
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
