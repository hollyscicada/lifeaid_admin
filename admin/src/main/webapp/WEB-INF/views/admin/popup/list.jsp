<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
<c:set var="name" value="popup" />
<c:set var="seq" value="popup_seq" />   
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(e){
    				location.href="/supervise/popup/update/"+$(this).attr("data-seq");
    		})
    		setTimeout(function(){
				$("#popup").fadeIn(1000);
			},500)
			$(".btn-add").click(function(){
				location.href="/supervise/popup/create"
					cosole.log("error");
			});

			/* popup 수정 /삭제 버튼	*/    
			$(".p_update").click(function(){
				if(confirm("수정하시겠습니까?")){
					var seq = $(this).attr("data-seq");
					location.href="/supervise/popup/update/"+seq;
				}
			});
			$(".btn-danger").click(function(){
				if(confirm("팝업을 삭제하시겠습니까?")){
					var seq = "${detail.popup_seq }";
					ajaxCallGet("/supervise/admin/v1/popup/delete?seq="+seq, function(res){
						if(res.success){
							alert("삭제되었습니다.");
							location.href="/supervise/popup/list"
						}
					})
				}
			});
		})
		
		function DataSearch(){
				var search_value = $("input[name=search_value]");
				var search_type = $("input[name=search_type]");

				var param={
						"search_value" : search_value.val(),
						"search_type" : search_type.val()
				};
				
				ajaxCallPost("/supervise/admin/v1/popup/search",param, function(res){
					if(res.success){
						var ss = "";
					for(var i = 0; i < res.data.length ; i++){

						ss +=  `<tr>
	                       <td><input type="checkbox" id="chk" name="chk" class="chk" value="`+res.data[i].popup_seq+`"/></td>
						   <td>`+res.data[i].popup_seq+`</td>
	                       <td class="go-detail-item" data-seq="`+res.data[i].popup_seq+`">`+res.data[i].title+`</td>
	                       <td>`+res.data[i].device_code+`</td>
	                       <td>`+res.data[i].start_dt+` ~ `+res.data[i].end_dt+`</td>
	                       <td>`+res.data[i].p_width+` ~ `+res.data[i].p_height+`</td>
	                       <td>`+res.data[i].x_location+` ~ `+res.data[i].y_location+`</td>
	                       <td>`+res.data[i].show_yn+`</td>
	                   </tr>`;
	                   $(".tbody").html(ss)
						
					}
					$(".listlength").html(res.data.length)
					

					alert("조회되었습니다.");
					}
				},function(){})
			}
    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>팝업 관리</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                    <div class="row">
                                    	<div>
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
            									<th>기본검색</th>
            								</tr>
            								<tr>
            								<tr>		
            									<th>검색어 입력</th>
            									<td>		            									
            											<select id="search_type" class="form-control">
		            										<option value="all">전체</option>
		            										<option value="title">제목</option>
		            										<option value="device_code">접속기기</option>
		            									</select>
		            							
		            							<td colspan="6">			
		            								<input type="text" name="search_value" class="form-control" >
		            							</td>
            								</tr>
            							</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four " style="float:right; margin-left:10px" onclick="DataSearch()">검색</button>
										<button type="reset" class="btn btn-primary " style="float:right;">초기화</button>
								 </form>    
                               </div>
                               
                               
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px">
                                  		<h4 >조회 건수 : <span class="listlength">${fn:length(list)}</span> 건</h4>
							        </div>
                                    <div class="row">
                                   
                                     <table id="popup" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                               <thead>
                                            <tr>
                                            	<th><input type="checkbox" id="chkAll" name="chkAll" class="chkAll"/></th>
                                            	<th>번호</th>
                                            	<th>제목</th>
                                            	<th>접속기기</th>
                                            	<th>실행기간</th>
                                            	<th>팝업크기(width X height)</th>
                                            	<th>위치좌표(x - y)</th>
                                            	<th>노출</th>
                                              	<th>관리</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${list }" var="item" >
                                            	<c:set var="temp_seq" value="${item.popup_seq }" />
                                           	   <tr>
	                                                <td><input type="checkbox" id="chk" name="chk" class="chk" value="${item.popup_seq }"/></td>
	                                                <td>${item.popup_seq }</td>
	                                                <td class="go-detail-item" data-seq="${item.popup_seq}">${item.title }</td>
	                                                <td>${item.device_code }</td>
	                                                <td>${item.start_dt } ~ ${item.end_dt }</td>
	                                                <td>${item.p_width } ~ ${item.p_height }</td>
	                                                <td>${item.x_location } ~ ${item.y_location }</td>
													<%@ include file="/WEB-INF/views/admin/common/showYn.jsp" %>
													<td><button type="button" class="btn p_update">수정</button></td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                    	<div style="float:left">
		            					<button type="button" class="btn btn-primary btn-remove">선택삭제</button>
		            					</div>
		            					<div style="float:right">
		            					<button type="button" class="btn btn-primary btn-add">+추가하기</button>
										</div>                            	
                            		</div>
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Static Table End -->
