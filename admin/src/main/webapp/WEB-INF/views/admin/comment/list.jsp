<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 

    <script>
    	$(function(){
    		
    		setTimeout(function(){
				$("#comment").fadeIn(1000);
			},500)
		    $(document).ready(function(){
		    	
		    	$('ul.comment-tabs li').click(function(){
		    		var tab_id = $(this).attr('data-tab');

		    		$('ul.comment-tabs li').removeClass('current');
		    		$('.comment-tab-content').removeClass('current');

		    		$(this).addClass('current');
		    		$("#"+tab_id).addClass('current');
		    	})

		    })
		})
		
		function DataSearch(){
				var start_1 = $("input[name=start_1]");
				var finish_1 = $("input[name=finish_1]");

				var delete_yn = $("#delete_yn").val();
				var member_seq = $("#member_seq").val();
				var search_value = $("input[name=search_value]");
				var search_type = $("input[name=search_type]");

				if (start_1==""||finish_1==""){
					alert("검색 날짜를 입력하세요 !");
					return;
				}

				var param={
						"start_1" : start_1.val(),
						"finish_1" : finish_1.val(),
						"delete_yn" : delete_yn,
						"member_seq" : member_seq,
						"search_value" : search_value.val(),
						"search_type" : search_type.val()
				};
				
				ajaxCallPost("/supervise/admin/v1/comment/search",param, function(res){
					if(res.success){
						var ss = "";
					for(var i = 0; i < res.data.length ; i++){

						ss +=  `<tr>
							<td>`+res.data[i].comment_seq+`</td>
							<td>`+res.data[i].member_seq+`</td>
							<td>`+res.data[i].content_seq+`</td>
							<td>`+res.data[i].content+`</td>
						</tr>`;
						$(".tbody").html(ss)
						
					}
					$(".listlength").html(res.data.length)
					

					alert("조회되었습니다.");
					}
				},function(){})

		}

    </script>
    
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>댓글 관리</h1>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-botton: 50px;"> 
                            <div class="comment-tab-bar">
                            	<ul class="comment-tabs">
                            		<li class="tab-link current" data-tab="tab-1">작성기간으로 조회</li>
                            		<li class="tab-link" data-tab="tab-2">댓글 내용 검색</li>
                            		<li class="tab-link" data-tab="tab-3">작성 계정 검색</li>
                            		<li class="tab-link" data-tab="tab-4">삭제된 댓글</li>
                            		<li class="tab-link" data-tab="tab-5">차단된 계정</li>
                            	</ul>
                            	<div id="tab-1" class="comment-tab-content current">
                             		 	<p>언제 작성된 댓글을 조회할까요? </p>
                           		   <form style="top:50%;">
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr style="height:50%;">
            									<th>조회 기간</th>
            									<td colspan="3">
            										<input type="date" class="datepicker form-control-sm" name="start_1"/>
            										<a href="#"></a> ~ 
            										<input type="date" class="datepicker form-control-sm" name="finish_1"/>
												</td>
            								</tr>
            							</table>
			                   		</form>                          		
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px" onclick="DataSearch()">조회</button>
                            		</div>
                            	
                            	<div id="tab-2" class="comment-tab-content">
									<form>
										<input type="hidden" name="search_type" value="content">
										<input type="text" name="search_value" class="form-control" placeholder="검색하실 단어, 혹은 짧은 문장을 입력해주세요."/>
										<button type="button" class="btn form-control" name="" onclick="DataSearch()">조회</button>
									</form>
								</div>   
                            	<div id="tab-3" class="comment-tab-content">
									<form>
										<input type="hidden" name="search_type" value="member_seq">
                            			<input type="text" name="search_value" class="form-control" placeholder="검색하실 계정의 이름을 입력해주세요."/>
										<button type="button" class="btn form-control" onclick="DataSearch()">검색</button>
									</form>
								</div>
       							<div id="tab-4" class="comment-tab-content">
                             		 	<p>언제 삭제된 댓글을 조회할까요? </p>
                            	  <form>
                            	  <table id="select" class="table table-bordered table-striped x-editor-custom" >
                             				<tr>
            									<th>조회 기간</th>
            									<td >
            										<input type="date" class="datepicker form-control-sm" name="start_1"/>
            										<a href="#"></a> ~ 
            										<input type="date" class="datepicker form-control-sm" name="finish_1"/>
												</td>
            								</tr>
            							</table>
			                 		  </form>       		
                            	
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px">조회</button>
                            	</div>
                            	
                            	<div id="tab-5" class="comment-tab-content">
                             		 	<p>언제 차단된 댓글을 조회할까요? </p>
                             		 <form>	
                             		 <table id="select" class="table table-bordered table-striped x-editor-custom" >
                             				
            								<tr>
            									<th>조회 기간</th>
            									<td >
            										<input type="date" class="datepicker form-control-sm" name="start_1"/>
            										<a href="#"></a> ~ 
            										<input type="date" class="datepicker form-control-sm" name="finish_1"/>
												</td>
            								</tr>
            							</table>	
			                   		</form>            		
                                    	<button type="button" class="btn btn-custon-four btn-success" style="float:right; margin-left:10px">조회</button>
                            	</div>
                             </div>
                             
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
									<h4 >조회 건수 : <span class="listlength">${fn:length(list)}</span> 건</h4>
                                    <table id="comment" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                            <thead>
                                            <tr>
                                            	<th>번호</th>
                                            	<th>회원번호</th>
                                            	<th>게시글번호</th>
                                            	<th>댓글내용</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody">
                                            <c:forEach items="${list }" var="item" >
                                           	   <tr>
	                                                <td>${item.comment_seq }</td>
	                                                <td>${item.member_seq }</td>
	                                                <td>${item.content_seq }</td>
	                                                <td>${item.content }</td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
   					  </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <!-- Static Table End -->
