<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	$(function(){
		$(".submit-btn").click(function(){
			var member_id = $("input[name=member_id]");
			var password = $("input[name=password]");
			var write_by = $("input[name=write_by]");
			var premium_seq = $("input[name=premium_seq]");
			var phone = $("input[name=phone]");

			
			var param = {
					"member_id" : member_id.val(),
					"password" : password.val(),
					"write_by": write_by.val(),
					"phone" : phone.val(),
					"premium_seq" : premium_seq.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/member/create", param, function(res){
				if(res.success){
					alert("회원 등록이 완료되었습니다.");
					location.href="/supervise/member/list"
				}
			},function(){})
		})
	})
	
	$(function(){
		$(".cancel-btn").click(function(){
			location.href="/supervise/member/list"
		})
	})
	
	$(function(){
		$(".check-btn").click(function(){
			var url="/supervise/member/create";
			var name="popup";
			var option="width=500, height=500";

			window.open(url, name, option);
				
			})
	})
	
	
	
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



		<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="card"  style="margin-top:50px; margin-bottom:30px;">
                                <div class="card-header">
                                    <h1 class="card-title">회원 추가 / 수정</h1>
                                </div>
                            </div>
						<form>
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
						<div class="row">
								<div style="margin-left:30px; margin-right:30px;">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                       
			                                <tbody>
			                                	<tr>
													<th>회원 ID *</th>
													<td><input name="member_id" type="text" class="form-control" placeholder="아이디"></td>
												</tr>
			                                    <tr>
			                                        <th>비밀번호 *</th>
													<td><input type="password" name="password" class="form-control" value="비밀번호"></td>
												</tr>
			                                   <tr>	
													<th>비밀번호 확인 *</th>
													<td><input type="password" name="password" class="form-control" value="비밀번호"></td>
						                       </tr>
			                                    <tr>
													<th>관심 카테고리 *</th>
													<td class="form-control">
													<input name="category_seq" type="radio" value="1" checked>
														<span>운동</span>
														<input name="category_seq" type="radio" value="2">
														<span>체형교정</span>
														<input name="category_seq" type="radio" value="3">
														<span>마사지/스트레칭</span>
														<input name="category_seq" type="radio" value="4">
														<span>재활/물리치료</span>
													</td>
												</tr>
			                                    <tr>
			                                        <th>휴대전화</th>
													<td><input name="phone" type="text" class="form-control" placeholder="- 없이 입력"></td>
												</tr>
			                                    <tr>
			                                        <th>회원등급 *</th>
													<td class="form-control">
														<input name="premium_seq" type="radio" value="1" checked>
														<span>일반회원</span>
														<input name="premium_seq" type="radio" value="2">
														<span>프리미엄회원 (1개월)</span>
														<input name="premium_seq" type="radio" value="3">
														<span>프리미엄회원 (3개월)</span>
													</td>
												</tr>
			                                </tbody>
			                            </table>
								</div>
							</div>
						</form>
							<div style="float:right;">
								<button type="button" class="btn btn-primary waves-effect waves-light submit-btn">저장</button>
								<button type="button" class="btn btn-primary waves-effect waves-light cancel-btn">취소</button>
							</div>			
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->


