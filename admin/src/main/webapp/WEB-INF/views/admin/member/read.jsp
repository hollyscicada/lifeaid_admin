<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
			$(".btn-success").click(function(){
				if(confirm("정말 수정하시겠습니까?")){
					var seq = "${detail.member_seq }";
					location.href="/supervise/member/update/"+seq;
				}
			})
			$(".btn-danger").click(function(){
				if(confirm("정말 삭제하시겠습니까?")){
					var seq = "${detail.member_seq }";
					ajaxCallGet("/supervise/admin/v1/member/remove?seq="+seq, function(res){
						if(res.success){
							alert("작업이 완료되었습니다.");
							location.href="/supervise/member/list"
						}
					})
				}
			})
			$(".btn-list").click(function(){
					location.href="/supervise/member/list"
			})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 30px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div style="padding: 0 50px 0 0">
                                    <h1>회원상세</h1>
                                </div>
                            </div>
						<div style="padding: 30px 30px 0 30px">
							<table class="table table-bordered table-striped x-editor-custom">
			                      <tbody>
			                                	<tr>
													<th>회원번호</th>
													<th>회원ID</th>
													<th>회원등급</th>
													<th>구독기간</th>
													<th>휴대전화</th>
													<th>진행상태</th>
													<th>회원가입일시</th>
													<th>마지막로그인</th>
												</tr>
			                                    <tr>
			                                        <td>${detail.member_seq}</td>
			                                        <td>${detail.member_id}</td>
			                                        <td>${detail.premium_yn}</td>
			                                        <td>${detail.premium_start_dt} ~ ${detail.premium_end_dt}</td>
			                                        <td>${detail.phone}</td>
			                                        <td>${detail.phone}</td>
			                                        <td>${detail.create_dt_yyyymmdd }</td>
			                                        <td>${detail.phone }</td>
			                                    </tr>
	                                </tbody>
		                    </table>
						
						</div>						
						<div style="padding: 30px 30px 0 30px">
							<form>
								<div class="row">
									<div>
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                              <caption>- 상세 회원 정보</caption>
			                                <tbody>
			                                	<tr>
													<th>회원 ID</th>
													<td>${detail.member_id }</td>
													<th>관심카테고리</th>
													<td>${detail.member_id }</td>
																										
												</tr>
			                                    <tr>
			                                        <th>비밀번호</th>
													<td>${detail.password }
													<input type="button" name="password" value="비밀번호 재발급">
													</td>
												</tr>
			                                    <tr>
			                                        <th>휴대전화</th>
													<td>${detail.phone }</td>
												</tr>
			                                    <tr>
													<th>회원가입일시</th>
													<td>${detail.create_dt_yyyymmdd }</td>
													<th>마지막 접속일</th>
													<td>${detail.login_history }</td>
												</tr>
			                                </tbody>
			                            </table>
									</div>

								</div>
							</form>
						</div>
						<div style="padding: 0 30px 50px 30px">
						<button type="button" class="btn btn-custon-four btn-list" style="float:right;">목록</button>
						<button type="button" class="btn btn-custon-four btn-danger" style="float:right; margin-right:10px">삭제</button>
						<button type="button" class="btn btn-success" style="float:right; margin-right:10px">수정</button>
						</div>
					</div>
					<div style="padding: 30px 30px 0 0">
						<table class="table table-bordered table-striped x-editor-custom">
			             <caption>- 프리미엄 구독</caption>
			                    <tbody>
			                                	<tr>
													<th>결제일시</th>
													<th>결제유형</th>
													<th>결제금액</th>
													<th>결제방법</th>
													<th>상태/승인번호</th>
													<th>구독일시</th>
												</tr>
			                                    <tr>
			                                        <td>${payment.payment_dt_yyyymmdd }</td>
			                                        <td>${payment.payment_type}</td>
			                                        <td>${payment.price}</td>
			                                        <td>${payment.payment_type}</td>
			                                        <td>${payment.status_code}</td>
			                                        <td>${payment.premium_start_dt}</td>
			                                    </tr>
			                      </tbody>
			             </table>
					
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
