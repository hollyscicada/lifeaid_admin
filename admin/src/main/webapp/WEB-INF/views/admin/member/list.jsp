<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
    
    <script>
    	$(function(){
    		$(document).on("click", ".go-detail-item", function(){
    			location.href="/supervise/member/"+$(this).attr("data-seq")
    		})
    		setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
			setTimeout(function(){
				$("#select").fadeIn(1000);
			},500)
			
			$(".btn-add").click(function(){
					location.href="/supervise/member/create"
			})		    

			$( ".chkAll" ).click(function(){
				var chk= $(this).is(":checked");

				if(chk)$("")

				
				$("[name='chk']").prop("checked", $(this).is(":checked"));
	
			});
	
    	})
    	
    	
    	function premium_up(count){
			var count;
			var member_seq = $("input[name=chk]");
			var premium_yn = $("#premium_yn").val();
			
			var param ={
				"premium_seq": count,
				"premium_yn" : premium_yn,
				"member_seq": member_seq.val()
			}
        	
			ajaxCallPost("/supervise/admin/v1/member/update_pre", param, function(res){
				if(res.success){
					alert("회원권을 변경하였습니다.");
					location.href="/supervise/member/list"
				}
			},function(){})

        }
    	
		/**/
    	function DataSearch(){

			var start_1 = $("input[name=start_1]");
			var finish_1 = $("input[name=finish_1]");

			var delete_yn = $("#delete_yn").val();
    		var premium_yn = $("#premium_yn").val();
			var search_value = $("input[name=search_value]");
			var search_type = $("#search_type").val();


			var param={
					"start_1" : start_1.val(),
					"finish_1" : finish_1.val(),
					"delete_yn" : delete_yn,
					"premium_yn" : premium_yn,
					"search_value" : search_value.val(),
					"search_type" : search_type,
			};
			

			ajaxCallPost("/supervise/admin/v1/member/search",param, function(res){
				if(res.success){
					var ss = "";
				for(var i = 0; i < res.data.length ; i++){

					  ss +=  `<tr>
	                       <td><input type="checkbox" id="chk" name="chk" class="chk" value="`+res.data[i].member_seq+`"/></td>
						   <td>`+res.data[i].member_seq+`</td>
	                       <td class="go-detail-item" data-seq="`+res.data[i].member_seq+`">`+res.data[i].member_id+`</td>
	                       <td>`+res.data[i].premium_seq+`</td>
	                       <td>`+res.data[i].create_dt_yyyymmdd+`</td>
	                       <td>`+res.data[i].phone+`</td>
	                       <td>`+res.data[i].create_dt_yyyymmdd+`</td>
	                       <td>`+res.data[i].phone+`</td>
	                       <td>${phone }</td>
	                   </tr>`;
	                   $(".tbody").html(ss)
				}
				
				$(".listlength").html(res.data.length)
				alert("조회되었습니다.");
				}
				
			},function(){})

        }
	


    </script>
  
        <!-- Static Table Start -->
        <div class="all-content-wrapper" style="min-height: 900px;background: white;">
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row" style="padding-top:30px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div style="padding-bottom:30px;">
                                    <h1>전체 회원 리스트</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                   <form>
                                    <div class="row">
                                    	<div>
                                    	<table id="select" class="table table-bordered table-striped x-editor-custom" >
                             
            								<tr>
            									<th>가입 일자</th>
            									<td colspan="3">
            										<input type="date" class="datepicker form-control-sm" name="start_1"/>
            										<a href="#"></a> ~ 
            										<input type="date" class="datepicker form-control-sm" name="finish_1"/>
												</td>
            										
            									<th>상태 구분</th>
            									<td colspan="3">
            									<select id="delete_yn" name="delete_yn" class="form-control" >
            										<option value="">전체</option>
            										<option value="N">정상</option>
            										<option value="Y">탈퇴</option>
            									</select>
            									</td>
            								</tr>
            								<tr>		
            									<th>최근 로그인 일자</th>
            									<td colspan="3">
														<input type="date" class="datepicker form-control-sm" name="start_2"/>
														<a href="#"></a> ~ 
														<input type="date" class="datepicker form-control-sm" name="finish_2"/>
													</td>
            									<th>회원등급 구분</th>
            									<td colspan="3">
													<select id="premium_yn" name="premium_yn" class="form-control">
														<option value="">전체</option>
														<option value="N">일반회원</option>
														<option value="Y">프리미엄회원</option>
													</select>
												</td>
            					
            								</tr>
            								<tr>	
            									<th>서비스 카테고리 구분</th>
            									<td>
		            									<select id="category_1" class="form-control">
		            										<option value="all">카테고리 분류1</option>
		            										<c:forEach var="category" items="${category }" varStatus="state">
		            										<option value="${category.category_seq }">${category.name }</option>
		            										</c:forEach>
		            									</select>
		            							</td>
		            							<td>		
		            									<select id="category_2" class="form-control">
		            										<option value="all">카테고리 분류2</option>
		            										<option value="all">전체</option>
		            										<option value="y">정상</option>
		            										<option value="n">탈퇴</option>
		            									</select>
		            							</td>
		            							<td>
		            									<select id="category_3" class="form-control">
		            										<option value="all">카테고리 분류3</option>
		            										<option value="all">전체</option>
		            										<option value="y">정상</option>
		            										<option value="n">탈퇴</option>
		            									</select>
		            							<td>		
		            									<select id="category_4" class="form-control">
		            										<option value="all">카테고리 분류4</option>
		            										<option value="all">전체</option>
		            										<option value="N">정상</option>
		            										<option value="Y">탈퇴</option>
		            									</select>
            									</td>
            								</tr>
            								<tr>
            									<th>검색어 입력</th>
            									<td>		            									
            											<select id="search_type" name="search_type" class="form-control">
															<option value="all">검색어</option>
		            										<option value="id">회원 ID</option>
		            										<option value="phone">휴대전화</option>
		            									</select>
		            							
		            							<td colspan="6">			
		            								<input type="text" id="search_value" name="search_value" class="form-control" value="">
		            							</td>
            								</tr>
            							</table>
                                    </div>
                                    </div>		
                                    	<button type="button" class="btn btn-custon-four btn-success" onclick="DataSearch()" style="float:right; margin-left:10px">조회</button>
										<button type="reset" class="btn btn-primary btn-success" style="float:right;">초기화</button>
			
                               </form>    
                               </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:50px">
                                    	<input type="hidden" value="${member.member_seq}">
										<h4 >조회 건수 : <span class="listlength">${fn:length(list)}</span> 건</h4>
							        </div>
                                    <div class="row">
                                   
                                     <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-toolbar="#toolbar">
                         
			                               <thead>
                                            <tr>
                                            	<th><input type="checkbox" id="chkAll" name="chkAll" class="chkAll"/></th>
                                            	<th>회원번호</th>
                                            	<th>회원ID</th>
                                            	<th>회원등급</th>
                                            	<th>구독기간</th>
                                            	<th>휴대전화</th>
                                              	<th>진행상태</th>
                                            	<th>회원가입</th>
                                            	<th>마지막로그인</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody">
                                            <c:forEach items="${list }" var="item" >
                                           	   <tr>
	                                                <td><input type="checkbox" id="chk" name="chk" class="chk" value="${item.member_seq }"/></td>
													<td>${item.member_seq }</td>
	                                                <td class="go-detail-item" data-seq="${item.member_seq}">${item.member_id }</td>
	                                                <td><c:if test="${item.premium_seq ne null}">프리미엄회원</c:if>
	                                                	<c:if test="${item.premium_seq eq null}">일반회원</c:if></td>
	                                                <td>${item.premium_name }</td>
	                                                <td>${item.phone }</td>
	                                                <td>${item.phone }</td>
	                                                <td>${item.create_dt_yyyymmdd }</td>
	                                                <td>${item.login_history }</td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                    	<div style="float:left">
                                		선택 회원 구독
                          				<button type="button" class="btn btn-custon-four" onclick="premium_up(1)">프리미엄1개월</button>
		            					<button type="button" class="btn btn-custon-four" onclick="premium_up(2)">프리미엄3개월</button>	
		            					</div>
		            					<div style="float:right">
		            					<button type="button" class="btn btn-primary btn-add ">회원추가</button>
										</div>                            	
                            		</div>
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Static Table End -->
