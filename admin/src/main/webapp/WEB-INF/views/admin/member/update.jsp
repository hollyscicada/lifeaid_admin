<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
	$(function(){

		setTimeout(function(){
			$("#card").fadeIn(1000);
		},500)


		
		$(".submit-btn").click(function(){
			var admin_seq = $("input[name=write_by]");
			var member_id = $("input[name=member_id]");
			var password = $("input[name=password]");
			var write_by = $("input[name=write_by]");
			var phone = $("input[name=phone]");
			var premium_seq = $("input[name=premium_seq]");
			var category_seq = $("input[name=category_seq]");

			
			var param = {
					"member_id" : member_id.val(),
					"admin_seq" : admin_seq.val(),
					"password" : password.val(),
					"write_by": write_by.val(),
					"phone" : phone.val(),
					"premium_seq" : premium_seq.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/member/update", param, function(res){
				if(res.success){
					alert("회원 등록이 완료되었습니다.");
					location.href="/supervise/member/list"
				}
			},function(){})
		})
	})
	
	$(function(){
		$(".cancel-btn").click(function(){
			location.href="/supervise/member/list"
			})
	})
	
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



		<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="card" id="card">
                                <div class="card-header">
                                    <h1 class="card-title">회원 추가 / 수정</h1>
                                </div>
                            </div>
					
						<div id="dropzone1" class="card-body">
							<form class="dropzone dropzone-custom needsclick add-professors">
								<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
						
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<label class="form-label">회원 ID *</label>
											<input name="member_id" type="text" class="form-control"
												placeholder="아이디" value="${detail.member_id }">
										</div>
										<div class="form-group">
											<label class="form-label">비밀번호 *</label>
											<input name="password" type="password" class="form-control"
												placeholder="비밀번호" >
										</div>
										<div class="form-group">
											<label class="form-label">비밀번호 확인 *</label>
											<input name="password" type="password" class="form-control"
												placeholder="비밀번호">
										</div>

										<div class="form-group">
											<label class="form-label">관심 카테고리 *</label>
											<div class="form-control">
											<input name="category_seq" type="radio" value="1" checked>
											<span>운동</span>
											<input name="category_seq" type="radio" value="2">
											<span>체형교정</span>
											<input name="category_seq" type="radio" value="3">
											<span>마사지/스트레칭</span>
											<input name="category_seq" type="radio" value="4">
											<span>재활/물리치료</span>
											</div>
										</div>
										<div class="form-group">
											<label class="form-label">휴대전화</label>
											<input name="phone" type="text" class="form-control"
												placeholder="- 없이 입력" value="${detail.phone }">
										</div>
										<div class="form-group">
											<label class="form-label">회원등급</label>
												<div class="form-control">
												<input name="premium_seq" type="radio" value="1" checked>
												<span>일반회원</span>
												<input name="premium_seq" type="radio" value="2">
												<span>프리미엄회원 (1개월)</span>
												<input name="premium_seq" type="radio" value="3">
												<span>프리미엄회원 (3개월)</span>
											</div>
										</div>
									</div>

								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="payment-adress">
											<button type="button"
												class="btn btn-primary waves-effect waves-light submit-btn">저장</button>
											<button type="button"
												class="btn btn-primary waves-effect waves-light cancel-btn">취소</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
