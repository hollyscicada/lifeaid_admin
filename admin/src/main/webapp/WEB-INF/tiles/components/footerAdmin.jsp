<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 푸터 -->
<div class="footer-copyright-area">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="footer-copy-right">
				
					
					<p>
						체형교정 재활 운동 스트레칭까지
						건강위키백과 <a href="https://colorlib.com/wp/templates/">Colorlib</a>
							
							<button type="button" class="ios_btn">앱스토어에서 받기</button>
                            <button type="button" class="android_btn">플레이스토어에서 받기</button>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 푸터 -->