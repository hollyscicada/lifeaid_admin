<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<script>
	$(function(){
		var global_menu_tab = $(".global-menu-tab");
		for(var i = 0 ; i < global_menu_tab.length ; i++){
			var data_url = global_menu_tab.eq(i).attr("data-url").split("|");
			for(var x = 0 ; x < data_url.length ; x++){
				if(location.pathname.indexOf(data_url[x]) > -1 && data_url[x]){
					global_menu_tab.removeClass("active");
					global_menu_tab.find("a").attr("aria-expanded", "true");
					global_menu_tab.find("ul").removeClass("show");
					
					global_menu_tab.eq(i).addClass("active");
					global_menu_tab.eq(i).find("a").attr("aria-expanded", "true");
					global_menu_tab.eq(i).find("ul").addClass("show");
				}
			}
		}
	})
</script>
<style>
	.go-detail-item{
		cursor: pointer;
	}
	
</style>
<!-- 레프트 메뉴 -->
 <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
           <!--  <div class="sidebar-header">
                <a href="/supervise"><img class="main-logo" src="/resources/images/h_logo.png" alt="" style="height: 60px;width: 200px;" /></a>
                <strong><a href="index.html"><img src="/resources/admin/img/logo/logosn.png" alt="" /></a></strong>
            </div>
            --> 
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                         <li class="global-menu-tab" data-url="contact|notice|faq">
                               <a class="has-arrow" href="/">
								   <span class="educate-icon educate-home icon-wrap"></span>
								   <span class="mini-click-non">홈</span>
							</a></li>

                        <!-- 추가 -->
                         <li class="global-menu-tab" data-url="member">
                            <a class="has-arrow" href="/supervise/member/list">
								   <span class="educate-icon educate-department icon-wrap"></span>
								   <span class="mini-click-non">회원 관리</span>
								</a>
                            <ul class="submenu-angle" aria-expanded="true" style="display: none">
                                <li><a title="Dashboard v.1" href="/supervise/member/list"><span class="mini-sub-pro">전체회원리스트</span></a></li>
                                <li><a title="Dashboard v.1" href="/supervise/member/create"><span class="mini-sub-pro">회원신규등록</span></a></li>
                           </ul>
                        </li>
                        <li class="global-menu-tab" data-url="payment">
                            <a class="has-arrow" href="/supervise/payment/list">
								   <span class="educate-icon educate-department icon-wrap"></span>
								   <span class="mini-click-non">결제내역</span>
								</a>
                            <ul class="submenu-angle" aria-expanded="true" style="display: none">
                                <li><a title="Dashboard v.1" href="/supervise/payment/list"><span class="mini-sub-pro">결제리스트</span></a></li>
                            </ul>
                        </li>
                        <li class="global-menu-tab" data-url="contents|ebook">
                            <a class="has-arrow" href="/supervise/contents/list">
								   <span class="educate-icon educate-department icon-wrap"></span>
								   <span class="mini-click-non">컨텐츠관리</span>
								</a>
                            <ul class="submenu-angle" aria-expanded="true" style="display: none">
                                <li><a title="Dashboard v.1" href="/supervise/contents/list"><span class="mini-sub-pro">영상 컨텐츠관리</span></a></li>
                                <li><a title="Dashboard v.1" href="/supervise/ebook/list"><span class="mini-sub-pro">E-Book 컨텐츠관리</span></a></li>
                            </ul>
                        </li>
                        
                        
                        <!-- 추가 끝 -->
                         <li class="global-menu-tab" data-url="comment|faq|cs">
                            <a class="has-arrow" href="/supervise/comment/list">
								   <span class="educate-icon educate-department icon-wrap"></span>
								   <span class="mini-click-non">커뮤니티관리</span>
								</a>
                            <ul class="submenu-angle" aria-expanded="true" style="display: none">
                                <li><a title="Dashboard v.1" href="/supervise/comment/list"><span class="mini-sub-pro">댓글관리</span></a></li>
                                <li><a title="Dashboard v.1" href="/supervise/faq/list"><span class="mini-sub-pro">F A Q</span></a></li>
                               <li><a title="Dashboard v.1" href="/supervise/cs/list"><span class="mini-sub-pro">C/S 1:1문의</span></a></li>
                               <li><a title="Dashboard v.1" href="/supervise/banner/list"><span class="mini-sub-pro">배너관리</span></a></li>
                            </ul>
                        </li>
                        
                         <li class="global-menu-tab" data-url="popup|category|common">
                            <a class="has-arrow" href="/supervise/popup/list">
								   <span class="educate-icon educate-department icon-wrap"></span>
								   <span class="mini-click-non">설정</span>
								</a>
                            <ul class="submenu-angle" aria-expanded="true" style="display: none">
                                <li><a title="Dashboard v.1" href="/supervise/popup/list"><span class="mini-sub-pro">팝업관리</span></a></li>
                                <li><a title="Dashboard v.1" href="/supervise/category/list"><span class="mini-sub-pro">카테고리관리</span></a></li>
                                <li><a title="Dashboard v.1" href="/supervise/category/update"><span class="mini-sub-pro">카테고리순위</span></a></li>
                                <li><a title="Dashboard v.1" href="/supervise/policy/list"><span class="mini-sub-pro">약관관리</span></a></li>
                                <li><a title="Dashboard v.1" href="/supervise/info/list"><span class="mini-sub-pro">회사소개</span></a></li>
                            </ul>
                        </li>
                       <li class="global-menu-tab" data-url="admin">
                            <a class="has-arrow" href="/supervise/admin/list">
								   <span class="educate-icon educate-home icon-wrap"></span>
								   <span class="mini-click-non">관리자관리</span>
							</a>
                            <ul class="submenu-angle" aria-expanded="true" style="display: none">
                                <li><a title="Dashboard v.1" href="/supervise/admin/list"><span class="mini-sub-pro">관리자리스트</span></a></li>
                                <li><a title="Dashboard v.2" href="/supervise/admin/create"><span class="mini-sub-pro">관리자추가</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
<!-- 레프트 메뉴 -->
<script>
// $(".submenu-angle").show();
</script>
<!-- 상단메뉴 -->
		<div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="index.html"><img class="main-logo" src="/resources/images/h_logo.png" alt="" style="height: 60px;" /></a>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12"style="width: 100%;">
                                        <div class="header-top-menu tabl-d-n">
                                        	<ul style="float: left">
                                        	<a href="/"><img class="main-logo" src="/resources/images/h_logo.png" alt="" style="height: 60px;width: 200px;" /></a>
                                        	</ul>
                                            <ul class="nav navbar-nav mai-top-nav" style="float: right">
                                                <li class="nav-item"><a href="#" class="nav-link" style="color:black;font-size: 12px;">${sessionScope.name}님</a></li>
                                                <li class="nav-item"><a href="/supervise/logout" class="nav-link" style="color:#8d9498;font-size: 12px;"">로그아웃</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu start 
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <li><a data-toggle="collapse" data-target="#Charts" href="/supervise/admin/list">관리자관리<span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul class="collapse dropdown-header-top">
                                                <li><a href="/supervise/admin/list">관리자리스트</a></li>
                                                <li><a href="/supervise/admin/create">관리자추가</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li><a data-toggle="collapse" data-target="#Charts" href="/supervise/member/list">설문관리<span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul class="collapse dropdown-header-top">
                                                <li><a href="/supervise/member/list">고객정보 관리</a></li>
                                                <li><a href="/supervise/yesno/list">Yes/No 관리</a></li>
                                                <li><a href="/supervise/question/list">설문문안 관리</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li><a data-toggle="collapse" data-target="#Charts" href="/supervise/skin/list">결과데이터관리<span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul class="collapse dropdown-header-top">
                                                <li><a href="/supervise/skin/list">피부진단</a></li>
                                                <li><a href="/supervise/gene/list">유전자검사</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>-->
            <!-- Mobile Menu end -->

<!-- 상단메뉴 -->