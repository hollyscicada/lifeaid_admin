<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Life-aid 관리자시스템</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="format-detection" content="telephone=no" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="/resources/js/commonAjax.js"></script>
	<script src="/resources/js/common.js"></script>
	<!-- 
	<script type="text/javascript" src="/resources/ckeditor/ckeditor.js"></script>
 	-->
	<script src="https://www.jsviews.com/download/jsrender.js"></script>
    <script src="//cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
<!--     <link rel="stylesheet" href="/resources/admin/css/owl.carousel.css"> -->
<!--     <link rel="stylesheet" href="/resources/admin/css/owl.theme.css"> -->
<!--     <link rel="stylesheet" href="/resources/admin/css/owl.transitions.css"> -->

    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/main.css">
    <!-- educate icon CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/educate-custon-icon.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="/resources/admin/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="/resources/admin/css/calendar/fullcalendar.print.min.css">
    <link rel="stylesheet" href="/resources/admin/css/datapicker/datepicker3.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/style.css">
    <link rel="stylesheet" href="/resources/admin/css/form.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="/resources/admin/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
<!--     <script src="/resources/admin/js/vendor/modernizr-2.8.3.min.js"></script> -->
</head>

<body>
    <!-- Main wrapper  -->
		<tiles:insertAttribute name="headerAdmin" />
		<tiles:insertAttribute name="contentAdmin" />
		<tiles:insertAttribute name="footerAdmin" />

    <!-- jquery
		============================================ 
    <script src="/resources/admin/js/vendor/jquery-1.12.4.min.js"></script>
    -->
    <!-- datepicker
		============================================ -->
    <script src="/resources/admin/js/datepicker/datepicker-active.js"></script>
    <script src="/resources/admin/js/datepicker/jquery-ui.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="/resources/admin/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
<!--     <script src="/resources/admin/js/wow.min.js"></script> -->
    <!-- price-slider JS
		============================================ -->
    <script src="/resources/admin/js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="/resources/admin/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
<!--     <script src="/resources/admin/js/owl.carousel.min.js"></script> -->
    <!-- sticky JS
		============================================ -->
    <script src="/resources/admin/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="/resources/admin/js/jquery.scrollUp.min.js"></script>
    <!-- counterup JS
		============================================ -->
<!--     <script src="/resources/admin/js/counterup/jquery.counterup.min.js"></script> -->
<!--     <script src="/resources/admin/js/counterup/waypoints.min.js"></script> -->
<!--     <script src="/resources/admin/js/counterup/counterup-active.js"></script> -->
    <!-- mCustomScrollbar JS
		============================================ -->
<!--     <script src="/resources/admin/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> -->
<!--     <script src="/resources/admin/js/scrollbar/mCustomScrollbar-active.js"></script> -->
    <!-- metisMenu JS
		============================================ -->
    <script src="/resources/admin/js/metisMenu/metisMenu.min.js"></script>
    <script src="/resources/admin/js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
<!--     <script src="/resources/admin/js/morrisjs/raphael-min.js"></script> -->
<!--     <script src="/resources/admin/js/morrisjs/morris.js"></script> -->
<!--     <script src="/resources/admin/js/morrisjs/morris-active.js"></script> -->
    <!-- morrisjs JS
		============================================ -->
<!--     <script src="/resources/admin/js/sparkline/jquery.sparkline.min.js"></script> -->
<!--     <script src="/resources/admin/js/sparkline/jquery.charts-sparkline.js"></script> -->
<!--     <script src="/resources/admin/js/sparkline/sparkline-active.js"></script> -->
    <!-- calendar JS
		============================================ -->
<!--     <script src="/resources/admin/js/calendar/moment.min.js"></script> -->
<!--     <script src="/resources/admin/js/calendar/fullcalendar.min.js"></script> -->
<!--     <script src="/resources/admin/js/calendar/fullcalendar-active.js"></script> -->
    <!-- plugins JS
		============================================ -->
<!--     <script src="/resources/admin/js/plugins.js"></script> -->
    <!-- main JS
		============================================ -->
    <script src="/resources/admin/js/main.js"></script>
    <!-- tawk chat JS
		============================================ -->
<!--     <script src="/resources/admin/js/tawk-chat.js"></script> -->
</body>

</html>
